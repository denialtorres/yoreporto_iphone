angular.module('yociudadano')

.factory('User', function(
	$http,
	$ionicLoading,	
	$q, 
	$localstorage, 
	SERVER, 
	CLIENTS, 
	$cordovaOauth,
	$cordovaOauthUtility,
	$twitterApi,	
	$state, 
	$ionicPopup,
	$rootScope,
	$timeout
) {



	
	
	var o = {
		session_id: false,
		name: false,
		username: false,
		since: false, 
		image: false,
		imagetwitter: false, 
		reports: false,
		comments: false,
		level: false,
		fbAccessToken: false,
		twitterInfo: false,
		fbInfo: false,
		isAFacebookUser: false,
		isATwitterUser: false,
		provider: false,
		unreadCount: false,
		user_id: false

		
	};

    

    o.notificaciones = function(){
    	 o.unreadCount = o.unreadCount + 1;
  		return o.unreadCount;

    }

	o.getStatistics = function() {
		var req = {
			method: 'GET',
			url: SERVER.url + '/api/v1/user/stats',
			headers: {
				'SessionId': o.session_id
			}
		}
		return $http(req).success(function(data) {
			// merge data
			o.reports = data.user.report_count;
			o.comments = data.user.comment_count;
			o.level = data.user.level;
		});
	}

//   ____                     _                   _                                   _                       __                            
//  / ___|_ __ ___  __ _  ___(_) ___  _ __     __| | ___   _   _ ___ _   _  __ _ _ __(_) ___     ___ _ __    / _| ___  _ __ _ __ ___   __ _ 
// | |   | '__/ _ \/ _` |/ __| |/ _ \| '_ \   / _` |/ _ \ | | | / __| | | |/ _` | '__| |/ _ \   / _ \ '_ \  | |_ / _ \| '__| '_ ` _ \ / _` |
// | |___| | |  __/ (_| | (__| | (_) | | | | | (_| |  __/ | |_| \__ \ |_| | (_| | |  | | (_) | |  __/ | | | |  _| (_) | |  | | | | | | (_| |
//  \____|_|  \___|\__,_|\___|_|\___/|_| |_|  \__,_|\___|  \__,_|___/\__,_|\__,_|_|  |_|\___/   \___|_| |_| |_|  \___/|_|  |_| |_| |_|\__,_|
                                                                                                                                         
//                       _            
//  _ __ ___  __ _ _   _| | __ _ _ __ 
// | '__/ _ \/ _` | | | | |/ _` | '__|
// | | |  __/ (_| | |_| | | (_| | |   
// |_|  \___|\__, |\__,_|_|\__,_|_|   
//          |___/                    	

	o.auth = function(username, password, signingUp, signinUpData) {
		
		$ionicLoading.show({
			duration: 30000,
			noBackdrop: true,
			template: '<img src="img/iconanimation.gif" height="100" width="100">'
			})
		var authRoute;
		var params;
		if (signinUpData) {
			params = {
				auth_api: {
					email: signinUpData.email,
					name: signinUpData.name,
					username: signinUpData.username,
					password: signinUpData.password,
					gender: signinUpData.gender,
					birthdate: signinUpData.birthdate
				}
			};
		} else {
			params = {};
		}

		if (signingUp) {
			authRoute = 'signup';
		} else {
			authRoute = 'signin';
		}

		var req = {
			method: 'POST',
			url: SERVER.url + '/api/v1/auth/' + authRoute,
			headers: {
				'Username': username,
				'Password': password
			},
			data: params
		};
		return $http(req).success(function(data) {
			//alert("funciono el login");
			//alert(JSON.stringify(data));
			o.setSession(data.session_id, data.user.name, data.user.username, data.user.since, data.user.avatar_url, data.user.report_count, data.user.comment_count, data.user.level, data.user.id);
			
		});
	}
	


	o.finishSignup = function(userdata) {
		
		var params = {
			auth_api: {
				username: userdata.username,
				birthdate: userdata.birthdate,
				gender: userdata.gender,
				email: o.fbInfo.email
			}				
		};

		var req = {
			method: 'POST',
			url: SERVER.url + '/api/v1/auth/finish_signup',
			headers: {
				'SessionId': o.session_id,
			},
			data: params
		};

		return $http(req).success(function(data) {
			
			o.setSession(data.session_id, data.user.name, data.user.username, data.user.since, data.user.avatar_url, data.user.report_count, data.user.comment_count, data.user.level);
		});
	}



//-------------------------------------Bloque de funciones para Facebook----------------------------------------
//  _____              _                 _    
// |  ___|_ _  ___ ___| |__   ___   ___ | | __
// | |_ / _` |/ __/ _ \ '_ \ / _ \ / _ \| |/ /
// |  _| (_| | (_|  __/ |_) | (_) | (_) |   < 
// |_|  \__,_|\___\___|_.__/ \___/ \___/|_|\_\
                                           

	o.fbSignIn = function() {
		$ionicLoading.show({
			duration: 6000,
			noBackdrop: true,
			template: '<img src="img/iconanimation.gif" height="100" width="100">'
			})
		return $cordovaOauth.facebook(CLIENTS.facebook, ["email"]).then(function(result) {
			o.fbAccessToken = result.access_token;
			////alert(JSON.stringify(result));
		}, function(error) {
			var messagere = /flow/
			if (!messagere.test(error)) {
				////alert("Error -> " + error);
			}
			console.log("Error -> " + error);
		});
	}

	o.fbGetData = function() {
		return $http.get('https://graph.facebook.com/v2.2/me', { params: { access_token: o.fbAccessToken, fields: 'id,name,email', format: 'json' } }).then(function(response) {
			////alert(response.data);
			////alert(response.data.name);
			////alert(response.data.id);
			////alert(response.data.email);
			o.fbInfo = response.data;
			
		});
	}
   //despues de recolectar datos del usuario en fb comparamos ? 
	o.fbAuth = function() {
		o.provider = 'facebook'; 
		var params;
        ////alert("estas dentro de fbAuth");
        if (o.fbInfo.email == null)
        	{
        		var mail = Math.random().toString(36).substr(2, 5);
        		o.fbInfo.email = mail +'@temporal.com';
        	}
		params = {
			auth_api: {
				email: o.fbInfo.email,
				name: o.fbInfo.name,
				picture: 'http://graph.facebook.com/' + o.fbInfo.id + '/picture'
			}
		};

	
		 
		var req = {
			method: 'POST',
			url: SERVER.url + '/api/v1/auth/signin',
			headers: {
				'Uid': o.fbInfo.id,
				'Provider': 'facebook'
			},
			data: params
		};

		////alert(req.data);
		return $http(req)
		.success(function(data) {
			o.isAFacebookUser = true;
			//alert("funciono login");
			//alert(JSON.stringify(data));
			o.setSession(data.session_id, data.user.name, data.user.username, data.user.since, data.user.avatar_url, data.user.report_count, data.user.comment_count, data.user.level, data.user.id);
		})
		.error(function(data, status) {
  		alert("error");
  
  		$ionicPopup.alert({
						title: '<b>YoCiudadano</b>',
						template: 'Credenciales no válidas, vuelve a intentar.</br>Tipo de error => ' + status + '</br>Posibles Causas: </br>El correo o nombre de usuario ya existe en la base de datos, por favor intenta con nuevos valores.',
						okText: 'Aceptar',
						okType: 'button-energized'
					})
		});

	}

// Bloque para twitter------------------------------------------
//  _____          _ _   _            
// |_   _|_      _(_) |_| |_ ___ _ __ 
//   | | \ \ /\ / / | __| __/ _ \ '__|
//   | |  \ V  V /| | |_| ||  __/ |   
//   |_|   \_/\_/ |_|\__|\__\___|_|   
                                  
		 var twitterKey = 'STORAGE.TWITTER.KEY';
		 var clientId = 'Npc5ideB1SzbwHn7492syZtVo';
		 var clientSecret = 'GkRjASfNwQilZvE373hfUSqQRIrAmU2nV6ZrG7zAMDZfyW8El9';
		 var myToken = '';
	o.twitterSignIn = function(){		
		$ionicLoading.show({
			duration: 30000,
			noBackdrop: true,
			template: '<img src="img/iconanimation.gif" height="100" width="100">'
			})
		return $cordovaOauth.twitter(clientId, clientSecret).then(function (succ) {
        myToken = succ;
        window.localStorage.setItem(twitterKey, JSON.stringify(succ));
        $twitterApi.configure(clientId, clientSecret, succ);
		$twitterApi.getUserData(myToken.screen_name,myToken.user_id); 
		},function(error) {
    console.log(error);
	});
	}

 //despues de recolectar datos del usuario en twitter comparamos ? 
	o.twitterAuth = function() {
		o.provider = 'twitter'; 
		var params;
		var mail = Math.random().toString(36).substr(2, 5);
		params = {
			auth_api: {
				email: mail +'@temporal.com',
				name: myToken.screen_name,
				picture: null 
				 
			}
		};		 
		var req = {
			method: 'POST',
			url: SERVER.url + '/api/v1/auth/signin',
			headers: {
				'Uid': myToken.user_id,
				'Provider': 'twitter'
			},
		
			data: params
		};
			
			return $http(req).success(function(data) {
		 	alert("se realizo el login");
		 	alert(JSON.stringify(data));
			o.isATwitterUser = true;
			o.setSession(data.session_id, data.user.name, data.user.username, data.user.since, data.user.avatar_url, data.user.report_count, data.user.comment_count, data.user.level, data.user.id);
		});
	}
	

//-------------------------------------------------------------
o.finishTwitterSignup = function(userdata) {

	o.twitterimage = $twitterApi.getData();
				$ionicPopup.alert({
						title: '<b>Ya casi!</b>',
						template: 'Confirma el correo que te acabamos de enviar para completar el registro',
						okText: 'Aceptar',
						okType: 'button-energized'
				})
	
		var params = {
			auth_api: {
				username: userdata.username,
				birthdate: userdata.birthdate,
				gender: userdata.gender,
				email: userdata.email,
				picture: o.twitterimage, 
			}				
		};

		var req = {
			method: 'POST',
			url: SERVER.url + '/api/v1/auth/finish_signup',
			headers: {
				'SessionId': o.session_id,
			},
			data: params
		};

		return $http(req).success(function(data) {
			
			o.setSession(data.session_id, data.user.name, data.user.username, data.user.since, data.user.avatar_url, data.user.report_count, data.user.comment_count, data.user.level);
		});
	
	
	};	

	// Set the session data
	o.setSession = function(session_id, name, username, since, image, reports, comments, level, user_id) {
		//alert('tu nombre de usuario es '+ username); 
		if (session_id) o.session_id = session_id;
		if (name) o.name = name;
		if (username) o.username = username;
		if (since) o.since = since;
		if(user_id) o.user_id = user_id;
		var facebookre = /facebook/;
		var localre = /system/;
		var twittere = /pbs.twimg.com/;  

	switch (true) {  
    	case facebookre.test(image):  
        		console.log('proveedor facebook');  
       		    o.provider = 'facebook';
        		o.image = image; 
        		o.username = username;
        		o.falso = username;
        		o.name = name;    
        	break;  
    	case localre.test(image):  
        		console.log('proveedor local');  
        		o.provider = 'local';
        		o.image = SERVER.url + image;
        		o.username = username;
        		o.falso = username;
        		o.name = name;    
        	break;  
   		case twittere.test(image):  
        		console.log('proovedor twitter');
        		o.provider = 'twitter';
        		o.imagetwitter = image
        		o.username = username;
        		o.falso = username;
        		o.name = name;    
       	  	break;  
    	default:  
    			console.log('nulo');
    			o.provider = 'nulo';  
        		o.image = 'img/missing.png'; 
        		o.username = username;
        		o.falso = username;
        		o.name = name;           
		}  		
		o.reports = reports;
		o.comments = comments;
		if (level) o.level = level;

		$localstorage.setObject('user', { session_id: session_id, name: name, username: username, since: since, comments: comments, reports: reports, image: image, twitterimage: o.imagetwitter, user_id: user_id});

	//registrar telefono en el servidor de notificaciones
      function onDeviceReady() {  
    //registrar cliente para mandar notificaciones desde celular
    console.log("vas a firmar");
       var pushConfig = {
        pushServerURL: "https://jbossunifiedpush1-denialtorres.rhcloud.com/ag-push",
        alias: username,
        android: {
          senderID: "189378147966",
          variantID: "a83fdb3b-5cc7-4d4d-a773-4b9c92343d71",
          variantSecret: "5b1b572a-564f-4b33-8063-10bcd0848d3f"
        },
        ios: {
          variantID: "f69b3cd9-6ffc-4588-976b-b321535f8086",
          variantSecret: "8cf7fbf7-d5f0-4dab-b07e-47f7223c624f"
        }
    };
    var statusList = $("#app-status-ul");
    statusList.append('<li>deviceready event received</li>');
    try {
      statusList.append('<li>registering </li>');
      push.register(onNotification, successHandler, errorHandler, pushConfig);
    } catch (err) {
      txt = "There was an error on this page.\n\n";
      txt += "Error description: " + err.message + "\n\n";
      alert(txt);
    }
  // alert('tu nombre de usuario es '+ username); 
   var usuario = username;
   var admin = "admin"
 
}
//Finaliza registro del celular
  function onNotification(e) {
  	//alert("recibiste notificaion");
  	//$rootScope.badgeNotification();
  	//alert($rootScope.medalla); 
   $rootScope.medalla = $rootScope.medalla + 1 ;
   $rootScope.badgeNotification($rootScope.medalla);
    $state.go('tab.activity'); 
   //  var statusList = $("#app-status-ul");
   //  // on android we could play the sound, if we add the Media plugin
   //  if (e.sound && (typeof Media != 'undefined')) {
   //    var media = new Media("/android_asset/www/" + e.sound + '.wav');
   //    media.play();
   //  }
   //  if (e.coldstart) {
   //    statusList.append('<li>----' + '</li>');
   //  }
   // statusList.append('<li>MESSAGE -> MSG: ' + e.alert + '</li>');
    //only on ios
    if (e.badge) {
      push.setApplicationIconBadgeNumber(successHandler, e.badge);
    }


}
  function successHandler() {
    // $("#app-status-ul").append('<li>success</li>');
    console.log("se envio notificacion");
  }
  
  function errorHandler(error) {
  	console.log("no se pudo enviar notificacion");
    // $("#app-status-ul").append('<li>error:' + error + '</li>');
  }
  document.addEventListener('deviceready', onDeviceReady, true);;
  //fin del registro 
}

//Finalizado del armado del usuario
//----------------------------------------------------------
// Check for available session
o.checkSession = function() {
		//alert("estas en checkSession");	

		var defer = $q.defer();

		if (o.session_id) {
			// if session is already initialized in the service
			defer.resolve(true);
		} else {
			// detect if there is a session in localstorage from previous use
			// if it is, pull it into service
			//alert("Un usuario ya tenia sesion anteiormente");
			var user = $localstorage.getObject('user');

			//alert("esto vale user "+ JSON.stringify(user));
		    //AQUI YA TIENES TU USER ID
			//alert(user.image);
			//buscar el proveedor en baso a la url de la imagen de la session.
			var stro = JSON.stringify("¿"+ user.image);
			var stri = stro.replace('¿','');
			//alert(stro);
			//alert(stri); 
			var facebookProvider = stri.search("graph.facebook");
			var localProvider = stri.search("/users/avatars/");
			console.log("Valor de facebookProvider " + facebookProvider);
			console.log("Valor de localProvider" + localProvider); 

			if(facebookProvider > 1){ 
					o.provider = 'facebook';
					o.falso = user.username;
				}else if (localProvider > 1) {
					o.provider = 'local';
				}else{
			//		alert('hola');
					o.provider = 'twitter';
				}

			if (user.session_id) {
				// if there is a user, lets get information from server
				//alert("aqui andas"); 
				//alert("Vas a mandar" + user.username)
				o.setSession(user.session_id, user.name, user.username, user.since, user.image, false, false, false, user.user_id)
				var facebookre = /facebook/
				if (facebookre.test(user.image)) {
					//alert("Aqui indaga si es un usuario de facebook");
					o.isAFacebookUser = true;
				}else{

					o.image= SERVER.url + stri.replace('"','');
					o.username = "camiseta";
					}
				o.getStatistics().then(function() {
					defer.resolve(true);
				}, function(reason) {
					o.destroySession();
					defer.resolve(false);
				});
			} else {
				// no user info in localstorage, reject

				defer.resolve(false);
			}
		}

		// alert("otra funcion");
		return defer.promise;
	}

	o.fbUser = function() {
		return o.isAFacebookUser;
	}

	o.destroySession = function() {
		$localstorage.setObject('user', {});
		o.session_id = false;
		o.name = false;
		o.username = false;
		o.since = false;
		o.image = false;
		o.reports = false;
		o.comments =  false;
		o.level = false;
		o.isAFacebookUser = false;
	}

	o.recoverPassword = function(email) {
		//alert("vas a enviar mail: " + email); 
		var req = {
			method: 'POST',
			url: SERVER.url + '/api/v1/auth/recover',
			data: {
				auth_api: {
					email: email
				}
			}
		};

		$http(req).
			then(function(response){
				var data = response.data;
				var status = response.status;
				//$scope.showReportRecoveryMessage()
		$ionicLoading.show({
           template: 'Se te ha enviado un mensaje de correo electrónico<br>con instrucciones para restablecer tu contraseña.',
           animation: 'fade-in',
           showDelay: 500,
           showBackdrop: false,
        });

        $timeout(function(){
    	  $ionicLoading.hide();
   			 },4000);  

			},
			function(data){
				//alert("error");
			})
	}

	o.changePicture = function(session_id, newImage) {
		var options = {
			fileKey: 'user_api[image]',
			headers: {
				'SessionId': session_id
			},
			params: {

			}
		};

		var url = SERVER.url + '/api/v1/user/change_picture';

		
		return	$cordovaFileTransfer.upload(url, newImage, options);
	}

	o.setImage = function(newImage) {
		o.image = newImage;
	}

	return o;
});