// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('yociudadano', [
  'ionic',
  'ionic.utils',
  'uiGmapgoogle-maps',
  'ngTwitter',
  'youtube-embed',
  'ngIOS9UIWebViewPatch',
  'ngImgur',
  'ionicLazyLoad',
  'ngAnimate',
  'ionic.native',
  'ngCordova'
  ])

.run(function($ionicPlatform, $rootScope) {
  

  $ionicPlatform.ready(function() {


      

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
//    StatusBar.hide();
    // then override any default you want
        // Enable background mode while track is playing
       // cordova.plugins.backgroundMode.enable();


       window.addEventListener('native.keyboardshow', function(){
        console.log("muestra teclado");
        document.querySelector('div.tabs').style.display = 'none';
        console.log("funciona");
        angular.element(document.querySelector('ion-content.has-tabs')).css('bottom', 0);
        angular.element(document.querySelector('.comment-bar.bar.bar-footer.bar-stable')).css('bottom', 0);
        angular.element(document.querySelector('.has-footer.has-tabs')).css('bottom', 0);
        });

        window.addEventListener('native.keyboardhide', function () {
          console.log("quita teclado");
        var tabs = document.querySelectorAll('div.tabs');
        angular.element(tabs[0]).css('display', '');
        angular.element(document.querySelector('.comment-bar.bar.bar-footer.bar-stable')).css('bottom', 50);
        angular.element(document.querySelector('.has-footer.has-tabs')).css('bottom', 93);
        });

        // window.plugins.nativepagetransitions.globalOptions.duration = 500;
        // window.plugins.nativepagetransitions.globalOptions.iosdelay = 350;
        // window.plugins.nativepagetransitions.globalOptions.androiddelay = 350;
        // window.plugins.nativepagetransitions.globalOptions.winphonedelay = 350;
        // window.plugins.nativepagetransitions.globalOptions.slowdownfactor = 4;
        // // these are used for slide left/right only currently
        // window.plugins.nativepagetransitions.globalOptions.fixedPixelsTop = 0;
        // window.plugins.nativepagetransitions.globalOptions.fixedPixelsBottom = 0;
    
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleLightContent();
    }


  });
//-------------------------------
})

.config(function($ionicConfigProvider) {
    $ionicConfigProvider.tabs.style('standard').position('bottom');
    $ionicConfigProvider.navBar.alignTitle('center');
    $ionicConfigProvider.backButton.text('').icon('icon-yc-back').previousTitleText(false);
    $ionicConfigProvider.scrolling.jsScrolling(false);
})



.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html',
    controller: 'TabsCtrl',
    // Do not load the state until User has been populated
    resolve: {
      populateSession: function(User) {
        return User.checkSession();
      }
    },
    onEnter: function($state, User, $window) 
    {
      User.checkSession().then(function(hasSession) {
        if (!hasSession) $window.location.href = 'index.html'; // $state.go('splash');
        // use this so that views aren't cached incomplete and can load successfully on
        // next login without refresh.
      });
    }
  })

  // Each tab hast its own nav history stack
  .state('tab.map', {
    url: '/map',
    views: {
      'tab-map': {
        templateUrl: 'templates/map.html',
        controller: 'MapCtrl'
      }
    },
    onEnter: function($ionicPlatform, Reports, User) {
      $ionicPlatform.ready(function() {
        if(window.StatusBar) {
          //StatusBar.hide();
        }
      });
    }
  })
// Mi Actividad-------------------------------
  .state('tab.activity', {
    url: '/activity',
    views: {
      'tab-activity': {
        templateUrl: 'templates/activity.html',
        controller: 'ActivityCtrl'
      }
    },
    onEnter: function($ionicPlatform) {
      $ionicPlatform.ready(function() {
        if(window.StatusBar) {
          StatusBar.show();
        }
      });
    }
  })
// Directorio-----------------------------------
  .state('tab.contacts', {
    url: '/contacts',
    views: {
      'tab-contacts': {
        templateUrl: 'templates/contacts.html',
        controller: 'ContactsCtrl'
      }
    },
    onEnter: function($ionicPlatform) {
      $ionicPlatform.ready(function() {
        if(window.StatusBar) {
          StatusBar.show();
        }
      });
    }
  })
// YoCTV-------------------------------------
  .state('tab.yctv', {
    url: '/yctv',
    views: {
      'tab-yctv': {
        templateUrl: 'templates/yctv.html',
        controller: 'YctvCtrl'
      }
    },
    onEnter: function($ionicPlatform, Video) {
      $ionicPlatform.ready(function() {
        if(window.StatusBar) {
          StatusBar.show();
        }



      });
    }
  })
 //--------------------Otra Cosa
   .state('tab.otracosa', {
    url: '/otracosa',
    views: {
      'tab-otracosa': {
        templateUrl: 'templates/otracosa.html',
        controller: 'OtracosaCtrl'
      }
    },
    onEnter: function($ionicPlatform) {
      $ionicPlatform.ready(function() {
        if(window.StatusBar) {
          StatusBar.show();
        }
      });
    }
  })
   

//--------------------Profile---------------------
  .state('tab.profile', {
    url: '/profile',
    views: {
      'tab-profile': {
        templateUrl: 'templates/profile.html',
        controller: 'PerfilCtrl'
      }
    },
    onEnter: function($ionicPlatform) {
      $ionicPlatform.ready(function() {
        if(window.StatusBar) {
          StatusBar.show();
        }
      });
    }
  })
//-----------------------------------About--------------
  .state('tab.about', {
    url: '/about',
    views: {
      'tab-profile': {
        templateUrl: 'templates/about.html',
        controller: 'AboutCtrl'
      }
    },
    onEnter: function($ionicPlatform) {
      $ionicPlatform.ready(function() {
        if(window.StatusBar) {
          StatusBar.show();
        }
      });
    }
  })


  .state('tab.terms', {
    url: '/terms',
    views: {
      'tab-profile': {
        templateUrl: 'templates/terms.html',
        controller: 'AboutCtrl'
      }
    },
    onEnter: function($ionicPlatform) {
      $ionicPlatform.ready(function() {
        if(window.StatusBar) {
          StatusBar.show();
        }
      });
    }
  })



  .state('tab.map_report_detail', {
    url: '/map_report_detail/:id',
    views: {
      'tab-map': {
        templateUrl: 'templates/report_detail.html',
        controller: 'ReportDetailCtrl'
      }
    },
    onEnter: function($ionicPlatform) {
      $ionicPlatform.ready(function() {
        if(window.StatusBar) {
          StatusBar.show();
          //StatusBar.hide(); 
        }
      });
    }
  })

  .state('tab.new_report', {
    url: '/map_new_report',
    views: {
      'tab-map': {
        templateUrl: 'templates/new_report.html',
        controller: 'NewReportCtrl'
      }
    },
    onEnter: function($ionicPlatform) {
      $ionicPlatform.ready(function() {
        if (window.StatusBar) {
          StatusBar.show();
        }
      });
    }
  })

  .state('tab.activity_report_detail', {
    url: '/activity_report_detail/:id',
    views: {
      'tab-activity': {
        templateUrl: 'templates/report_detail.html',
        controller: 'ReportDetailCtrl'
      }
    }
  })

  // Individual views
  .state('splash',  {
    url: '/',
    templateUrl: 'templates/splash.html',
    controller: 'SplashCtrl',
    onEnter: function($state, User) {
      //cordova.plugins.camerapreview.hide();
      User.checkSession().then(function(hasSession) {
        if (hasSession) $state.go('tab.map');
      });
    }
  })

  .state('signup', {
    url: '/signup',
    templateUrl: 'templates/signup.html',
    controller: 'SignupCtrl'
  })
    //STATE INTRO
  .state('intro', {
    url: '/intro',
    templateUrl: 'templates/intro.html',
    controller: 'IntroCtrl'
  })

  .state('finish_signup', {
    url: '/finish_signup',
    templateUrl: 'templates/finish_signup.html',
    controller: 'FinishSignupCtrl'
  })

  //Si se autentifica por twitter
  .state('finish_signupo', {
    url: '/finish_signupo',
    templateUrl: 'templates/finish_twitter.html',
    controller: 'FinishSignupCtrlo'
  })
  // If none of the above states are matched, use this as the fallback:
  $urlRouterProvider.otherwise('/');
})

.config(function (uiGmapGoogleMapApiProvider) {
  uiGmapGoogleMapApiProvider.configure({
    key: 'AIzaSyA47C4qJmfWX1vVgsj0plfElJPuBAnb46Y',
    v: '3.17',
    libraries: 'visualization'
  });
})

.constant('SERVER', {
  url: 'http://159.203.251.164/'
})

.constant('CLIENTS', {
  facebook: '1467523886874545',
  google:   '189378147966-1hjdhfauim5q51tf1ui5sf2slnirab5u.apps.googleusercontent.com',
  twitter: 'Npc5ideB1SzbwHn7492syZtVo',
  twitterSecret: 'GkRjASfNwQilZvE373hfUSqQRIrAmU2nV6ZrG7zAMDZfyW8El9'
  
})

   .directive('keyboardHandler', function ($window) {
        return {
        restrict: 'A',
        link: function postLink(scope, element, attrs) {
            angular.element($window).bind('native.keyboardshow', function() {
                element.addClass('tabs-item-hide');
                
            });

            angular.element($window).bind('native.keyboardhide', function() {
                element.removeClass('tabs-item-hide');
               
            });
        }
    };
})

