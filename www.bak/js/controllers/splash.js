angular.module('yociudadano')
/*
Controller for splash
*/
.controller('SplashCtrl', function($scope, $window, $ionicLoading, User, $state, $twitterApi, $ionicPopup, $http, $cordovaSocialSharing) {
     
	//
	$scope.session_id = User.session_id;

	$scope.data = {
		badgeCount: 4
	};
	 
    $ionicLoading.hide();
    User.username = "";
    User.password = ""; 

	$scope.login = function(username, password) {
		
		User.auth(username.toLowerCase(), password).then(function() {
			// Session is now set, redirect
			console.log('Controller reports successfull login.');
			$state.go('tab.map');
		}, function() {
			// Error handling
			$ionicLoading.hide();
			$ionicPopup.alert({
				title: '<b>YoCiudadano</b>',
				template: 'Credenciales no válidas, vuelve a intentar.',
				okText: 'Aceptar',
				okType: 'button-energized'
			})
		});
	}
	//-----------------------------------Bloque para Login mediante Facebook------------------------------------------------
	$scope.facebookLogin = function() {
		//alert("enter to the function User.fbSignIn");
		User.fbSignIn().then(function() {
		//alert("Saliendo de User.fbSignIn y entrando a User.fbGetData() ");
			User.fbGetData().then(function() {
		//alert("Saliendo de  User.fbGetData() y entrando a User.fbAuth()");
				$ionicLoading.show({
			noBackdrop: true,
			template: '<img src="img/iconanimation.gif" height="100" width="100">'
			})
				User.fbAuth()
				.success(function() {
					//alert("Detect if it is a sign in or sign up");
					//alert(User.username);
					// Detect if it is a sign in or sign up
						if (User.username) {
							$ionicLoading.hide() 
							console.log('Controller reports successfull social login.');
							$state.go('tab.map');

						} else {
						// Open finish signup modal
							$ionicLoading.hide() 
							console.log('Contorller reports this is a new user');
							$state.go('finish_signup');
							}
					}, function() {
					

					//$state.go('finish_signup');
				});
			}, function() {
				// alert('Could not get your Facebook data...');
			});
		}, function() {
			// alert('Could not sign you into Facebook...');
		});
	}

//---------------------------Bloque para login por parte de twitter--------------------------------------------------
	$scope.twtLogin = function (){
		User.twitterSignIn().then(function(){
				User.twitterAuth().then(function(){
					if (User.username){
						console.log('Controller reports successfull social login.');
						$state.go('tab.map');
					} else {
						$state.go('finish_signupo');
						console.log('Controller reports this is a new user');								
					}
				}, function() {
					$state.go('splash');
			});
		}, function() {
			// alert ('could not get sing you into twitter ...');
		});
		}	
//--------------------------------------------------------------------------------------------------------------------
	$scope.goToSignup = function() {
		$state.go('signup');
	}

	$scope.reportProblem = function(){
		var message = "<h1>De ser posible agrega una captura a tu reporte</h1>"
    $cordovaSocialSharing.shareViaEmail(message, "Reporte de problema en la aplicacion YoCiudadano", "dtorres@planjuarez.org", null, null, null)
    .then(function(result) {
      // Success!
    }, function(err) {
      // An error occurred. Show a message to the user
    });
	}

	$scope.recoverPassword = function() {
		$scope.data = {}
		   //alert("aqui");

            $ionicPopup.show({
              templateUrl: 'popup-template.html',
              title: 'Ingresa tu correo electronico',
              subTitle: 'Con el que te registraste a YoCiudadano',
              scope: $scope,
              buttons: [
                { text: 'Cancel', onTap: function(e) { return true; } },
                {
                  text: '<b>Enviar</b>',
                  type: 'button-positive',
                  onTap: function(e) {
                    return $scope.data.wifi || true;
                  }
                },
              ]
              }).then(function(res) {
                console.log('Tapped!', res);
             //   alert(res);
                User.recoverPassword(res);

              }, function(err) {
                console.log('Err:', err);
              }, function(msg) {
                console.log('message:', msg);
              });

	}


})