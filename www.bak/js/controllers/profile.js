angular.module('yociudadano')

.controller('PerfilCtrl', function($scope, $state, $window, User, $cordovaCamera, $ionicActionSheet, $ionicLoading, $rootScope, $ionicPopup, $ionicPlatform, $http, $cordovaSocialSharing) {
	//StatusBar.hide(); 

	$rootScope.markers = []; 
	console.log($rootScope.markers); 
	$scope.username = User.username;
	$scope.name = User.name;
	$scope.since = User.since;
	$scope.reports = User.reports;
	$scope.comments = User.comments;
	$scope.level = User.level;
	$scope.image = User.image;// + '?type=large';
	$scope.fbUser = !User.fbUser();
	$rootScope.bandera = 1; 
	//alert(JSON.stringify(User));
	//alert("estas en profilejs");
	//alert("tu proveedor es: "+ User.provider); 
	//alert("la url de tu imagen es" + User.image);
	//alert(User.imagetwitter); 
	console.log("Proovedor " + User.provider);
	if(User.username == '') $scope.username = User.falso;
	if(User.provider == 'facebook') $scope.image = User.image + '?type=large';
	if(User.provider == 'local'){
		$scope.image = User.image; 
	
	} 
	if(User.provider == 'twitter') {
		$scope.image = User.imagetwitter;
	}
	if(User.provider == 'nulo') $scope.image = 'img/missing.png';
	$scope.currentUserLevelColor = function(userLevel) {
		var color = '';
		if (userLevel >= 1 || userLevel <= 3) {
			color = 'bronze';
		} else if (userLevel >= 4 || userLevel <= 6) {
			color = 'siver'
		} else {
				color = 'gold';	
		}
		return color;
	}

	$scope.currentUserLevelStarsNumber = function(userLevel) {
		var stars = 0;
		if (userLevel <= 1 || userLevel == 4 || userLevel == 7) {
			stars = 1;
		} else if (userLevel == 2 || userLevel == 5 || userLevel == 8) {
			stars = 2;
		} else {
			stars = 3;
		}
		return stars;
	}

	$scope.signout = function() {
		//alert("esto vale User" + JSON.stringify(User)); 
		User.destroySession();
		//alert("esto vale User ahora " + JSON.stringify(User)); 
		// instead of using $state.go, we're going to redirect.
		// reason: we need to ensure views aren't cached.
		//StatusBar.hide();
		
		$window.location.href = 'index.html';
		//$state.go('splash'); 
	}

	$scope.goToReport = function() {
		 
		 var message = "<h1>De ser posible agrega una captura a tu reporte</h1>"
    $cordovaSocialSharing.shareViaEmail(message, "Reporte de problema en la aplicacion YoCiudadano", "dtorres@planjuarez.org", null, null, null)
    .then(function(result) {
      // Success!
    }, function(err) {
      // An error occurred. Show a message to the user
    });
	}

	$scope.goToTutorial = function() {
		$state.go('intro');
	}

	$scope.goToAbout = function() {
		$state.go('tab.about');
	}

	$scope.openTermsModal = function() {
		$state.go('tab.terms');
	}


	$scope.chooseImageSource = function() {
		// Reveal action sheet to select image source
		var sourceSheet = $ionicActionSheet.show({
			buttons: [{
				text: 'Cámara'
			}, {
				text: 'Galería de fotos'
			}],
			titleText: 'Elige una opción',
			cancelText: 'Cancelar',
			buttonClicked: function(index) {
				if (index === 0) {
					$scope.startChooseProfileImageProcess(Camera.PictureSourceType.CAMERA);
				} else if (index === 1) {
					$scope.startChooseProfileImageProcess(Camera.PictureSourceType.PHOTOLIBRARY);
				}
				return true;
			}
		});
	}

	$scope.startChooseProfileImageProcess = function(source) {
		$ionicPlatform.ready(function() {
			var options = {
				quality: 40,
				correctOrientation: true,
				mediaType: 0,
				sourceType: source
			};
			$cordovaCamera.getPicture(options).then(function(imageData) {
				$ionicLoading.show({
					template: 'Cambiando imagen'
				});
				User.changePicture(User.session_id, imageData).then(function(result) {
					// Release Backdrop
					$ionicLoading.hide();

					// Change shown image
					//alert("tu proveedor es: "+ User.provider); 
					//alert("la url de tu imagen es" + imageData);
					$scope.image = imageData;
					//	alert(JSON.stringify(result)); 
					User.setImage(imageData);
				}, function(reason) {
					// Release Backdrop
					$ionicLoading.hide();

					// Send error alert
					var errorAlert = $ionicPopup.alert({
						title: 'Error',
						template: 'No se pudo cambiar tu imagen, inténtalo más tarde.',
						okType: 'button-energized'
					});

					errorAlert.then(function(res) {
						console.log(res);
					});
				});
			}, function(reason) {
				// Something went wrong with da picture
				// Don't send alert, it will appear if user cancels operation
			});
		});
	}


 //--funcion de prueba
 		function TabsCtrl($scope){
 			$scope.$emit

 		}

}) 