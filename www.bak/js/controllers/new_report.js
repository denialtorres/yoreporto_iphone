	angular.module('yociudadano')

.controller('NewReportCtrl', function($scope, $stateParams, User, Reports, $state, $ionicPlatform, $ionicLoading, $ionicPopup, $timeout, $ionicActionSheet, $cordovaCamera) {
	// Initialize main variable
	$scope.newReportData = {};
	$scope.newReportData = Reports.newReport;
	$scope.newReportData.report_category_id = 1;
	//
	$scope.followBtnImgUrl  = '/img/filtros/accecibilidad_2.png';




		$scope.prendido = [{id: 0, imgUrl: "../www/img/filtros/alumbrado_2.png", name:"Alumbrado"},
												{id: 1, imgUrl: "../www/img/filtros/limpia_2.png", name: "Limpia"},
												{id: 2, imgUrl: "../www/img/filtros/pavimento_2.png", name: "Pavimento"},
												{id: 3, imgUrl: "../www/img/filtros/parques_2.png", name: "Parques"},
												{id: 4, imgUrl: "../www/img/filtros/accecibilidad_2.png", name: "Accesibilidad"},
												];
	$scope.apagado = [{id: 0, imgUrl: "../www/img/filtros/alumbrado_1.png", name:"Alumbrado"},
										{id: 1, imgUrl: "../www/img/filtros/limpia_1.png", name: "Limpia"},
										{id: 2, imgUrl: "../www/img/filtros/pavimento_1.png", name: "Pavimento"},
										{id: 3, imgUrl: "../www/img/filtros/parques_1.png", name: "Parques"},
										{id: 4, imgUrl: "../www/img/filtros/accecibilidad_1.png", name: "Accesibilidad"},
										];
		$scope.merchants = [{id: 0, imgUrl: "../www/img/filtros/alumbrado_1.png", name:"Alumbrado"},
												{id: 1, imgUrl: "../www/img/filtros/limpia_1.png", name: "Limpia"},
												{id: 2, imgUrl: "../www/img/filtros/pavimento_1.png", name: "Pavimento"},
												{id: 3, imgUrl: "../www/img/filtros/parques_1.png", name: "Parques"},
												{id: 4, imgUrl: "../www/img/filtros/accecibilidad_1.png", name: "Accesibilidad"},
												];
// $scope.prendido = [{id: 0, imgUrl: "../img/filtros/alumbrado_2.png", name:"Alumbrado"},
// 										{id: 1, imgUrl: "../img/filtros/limpia_2.png", name: "Limpia"},
// 										{id: 2, imgUrl: "../img/filtros/pavimento_2.png", name: "Pavimento"},
// 										{id: 3, imgUrl: "../img/filtros/parques_2.png", name: "Parques"},
// 										{id: 4, imgUrl: "../img/filtros/accecibilidad_2.png", name: "Accesibilidad"},
// 										];
// $scope.apagado = [{id: 0, imgUrl: "../img/filtros/alumbrado_1.png", name:"Alumbrado"},
// 								{id: 1, imgUrl: "../img/filtros/limpia_1.png", name: "Limpia"},
// 								{id: 2, imgUrl: "../img/filtros/pavimento_1.png", name: "Pavimento"},
// 								{id: 3, imgUrl: "../img/filtros/parques_1.png", name: "Parques"},
// 								{id: 4, imgUrl: "../img/filtros/accecibilidad_1.png", name: "Accesibilidad"},
// 								];
// $scope.merchants = [{id: 0, imgUrl: "../img/filtros/alumbrado_1.png", name:"Alumbrado"},
// 										{id: 1, imgUrl: "../img/filtros/limpia_1.png", name: "Limpia"},
// 										{id: 2, imgUrl: "../img/filtros/pavimento_1.png", name: "Pavimento"},
// 										{id: 3, imgUrl: "../img/filtros/parques_1.png", name: "Parques"},
// 										{id: 4, imgUrl: "../img/filtros/accecibilidad_1.png", name: "Accesibilidad"},
// 										];

	$scope.toggleImage = function(merchant) {
	/* the toggling logic */

	console.log($scope.merchants[merchant.id].name);
	if(merchant.imgUrl === $scope.prendido[merchant.id].imgUrl) {
							// Prendido ==> Apagado
              merchant.imgUrl = merchant.$backupUrl;
          } else {
							// Apagado == > Prendido
							//alert("gato");
							angular.forEach($scope.merchants, function(value, key){
								console.log(value.name)
								console.log(value.imgUrl);
								value.imgUrl = $scope.apagado[key].imgUrl;
							});
              merchant.$backupUrl = merchant.imgUrl;
              merchant.imgUrl = $scope.prendido[merchant.id].imgUrl;
          }
	};

	// Array to store pins in mimiMap
	miniMapMarkers = [];
	$scope.$on('$ionicView.beforeEnter'), function(){
		appointmentsService.getAllAppointments();

	};
	// MiniMap Icons
	var markerHover = {
	    url: 'img/marcador_hover-original.png',
	    // This marker is 20 pixels wide by 32 pixels tall.
	    size: new google.maps.Size(88,118),
		scaledSize: new google.maps.Size(44, 59),
	    // The origin for this image is 0,0.
	    origin: new google.maps.Point(0,0),
	    // The anchor for this image is the base of the flagpole at 0,32.
	    anchor: new google.maps.Point(22, 59)
	};

	// Declare function for mimiMap
	function initializeMiniMap() {
		var mapOptions = {
			center:  new google.maps.LatLng(31.7, -106.4),
			zoom: 15,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDefaultUI: true,
			styles: [
				{
					"featureType": "poi",
			        "elementType": "labels.icon",
			        "stylers": [
			            {
			                "visibility": "off"
			            }
			        ]
				}
			]
		};

		var map = new google.maps.Map(document.getElementById("new-report-map-container"),mapOptions);

		// Store the minimap on scope
		$scope.miniMap = map;

		var centerOfMap;
		var options = { timeout: 30000, enableHighAccuracy: true, maximumAge: 10000 };
		navigator.geolocation.getCurrentPosition(function(pos) {
			centerOfMap = new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude);
			$scope.miniMap.setCenter(centerOfMap);
			addMarkerToMiniMap(centerOfMap);

			//New Report Data: update latitude and longitude
			$scope.newReportData.latitude = pos.coords.latitude;
			$scope.newReportData.longitude = pos.coords.longitude;

		}, function(error) {
			alert('Unable to get location: ' + error.message + '\nPlease select it manually.');
		}, options);

		// Update location upon click
		google.maps.event.addListener(map, "click", function(event) {
			$scope.newReportData.latitude = event.latLng.lat();
			$scope.newReportData.longitude = event.latLng.lng();
		    deleteMarkersOnMiniMap();
		   // alert(4);
		    addMarkerToMiniMap(event.latLng);
		});
	}

	// Marker functions for minimap
	function addMarkerToMiniMap(location) {
	    var marker = new google.maps.Marker({
		    position: location,
	    	map: $scope.miniMap,
	    	icon: markerHover,
	    	animation: google.maps.Animation.DROP
	    });
	    miniMapMarkers.push(marker);
	}

	function setAllMap(map) {
		for (var i = 0; i < miniMapMarkers.length; i++) {
			miniMapMarkers[i].setMap(map);
		}
	}

	function clearMarkersOnMiniMap() {
		setAllMap(null);
	}

	function deleteMarkersOnMiniMap() {
	    clearMarkersOnMiniMap();
		miniMapMarkers = [];
	}

	// Redirect function
	$scope.redirectToNewReportDetail = function(r) {
		// $location.url('/tabs/map-report-detail/' + r);
		// GlobalOptions.justCreatedANewReport.set(r);
		$state.go('tabs.map');
	}
     //boton para localizar en minimapa
     	$scope.botonYo = function () {
        // alert(JSON.stringify(Reports.newReport));
        Reports.newReport.details = ""
		initializeMiniMap();
	};
	// Minimap
	initializeMiniMap();

	// Category function
	$scope.reportCategoryList = [
		{ id: 1, name: 'Limpia', icon: 'ion-trash-a' },
		{ id: 2, name: 'Pavimento', icon: 'icon-categorias-pavimento' },
		{ id: 3, name: 'Parques', icon: 'ion-leaf' },
		{ id: 4, name: 'Alumbrado', icon: 'ion-lightbulb' },
		{ id: 5, name: 'Camara', icon: 'icon-camara'}
	];

	$scope.newReportCategory = 'Limpia';
	$scope.setSelectedCategory = function(category) {
		$scope.newReportCategory = category.name;
		$scope.newReportData.report_category_id = category.id;
	};
	$scope.categoryIsSelected = function(category) {
		//alert("estas en selected")

		return ($scope.newReportCategory === category);
	};
	//-------------------------------------------------
	$scope.chooseImageSource = function() {
		// Reveal action sheet to select image source
		var sourceSheet = $ionicActionSheet.show({
			buttons: [{
				text: 'Cámara'
			}, {
				text: 'Galería de fotos'
			}],
			titleText: 'Elige una opción',
			cancelText: 'Cancelar',
			buttonClicked: function(index) {
				if (index === 0) {
					$scope.startNewReportProcess(1)
					//alert("regresas");
				} else if (index === 1) {

		//	alert("eligiste desde galeria");
					$scope.startNewReportProcess(Camera.PictureSourceType.PHOTOLIBRARY)
					//alert("regresas");
				}
				return true;
			}
		});
	}

//------------------



	//-------------------------------------------------------
	$scope.startNewReportProcess = function(source) {
		$state.go('tab.yctv');
		
		
	// 	alert("source es " + source);
	// 	$ionicPlatform.ready(function() {
	// 			if(window.cordova){

	// 			}
	// 		 var tapEnabled = false; //enable tap take picture
 //      var dragEnabled = false; //enable preview box drag across the screen
 //      var toBack = false; //send preview box to the back of the webview
 //      var ancho = screen.width;
 //      var largo = screen.height;
 //      var rect = {x: 0, y: 0, width: 100, height: 100};
 // cordova.plugins.camerapreview.startCamera(rect, "back", tapEnabled, dragEnabled, toBack);




	// 	});
	}

	//------------------------------------------------------
	 $scope.showReportRecoveryMessage = function() {

        $scope.loadingIndicator = $ionicLoading.show({
           template: '¡Tu reporte ha sido creado!<br> Está en espera de aprobación.',
           animation: 'fade-in',
           showDelay: 500,
           showBackdrop: false,
        });

    	$timeout(function(){
    	  $scope.loadingIndicator.hide();
   			 },3000);
  		};

	// On-screen functions
	$scope.submitReport = function(newReportData) {
		$ionicLoading.show({
			template: 'Creando reporte'
		});
		$ionicPlatform.ready(function() {

		//	alert("estos son los datos de lo que vas a subir" + JSON.stringify($scope.newReportData));
			Reports.submitNewReport(User.session_id, $scope.newReportData).then(function(result) {
				// Report status to model
				Reports.submitReportResult(true);

				// Release backdrop
				$ionicLoading.hide();

				// Send the user back to the map and into the detail of the new report
				// var finishedAlert = $ionicPopup.alert({
				// 	title: 'Gracias',
				// 	template: '¡Tu reporte ha sido creado! Está en espera de aprobación.',
				// 	okType: 'button-energized'
				// });

  //---------------------------------------------------------------------
  		 $scope.showLoading = function() {

        $scope.loadingIndicator = $ionicLoading.show({
           template: '¡Tu reporte ha sido creado!<br> Está en espera de aprobación.',
           animation: 'fade-in',
           showDelay: 500,
           showBackdrop: false,
        });

    	$timeout(function(){
    	  $scope.loadingIndicator.hide();
    	  Reports.newReport.details = ""
    	  $state.transitionTo('tab.map', {}, { reload: true });
   			 },3000);

    	// $state.transitionTo('tab.map', {}, {
    	// 	reload: true,
    	// 	delay: 3000 });

  		};

	//-------------------------------------------------------
				// var finishedAlert = $ionicLoading.show({
				//    template: '¡Tu reporte ha sido creado! Está en espera de aprobación.'
				// });
				// finishedAlert.then(function(res) {
				// 	// Need to find a way to refresh data
				// 	alert("entra animacion");
				// 	$state.transitionTo('tab.map', {}, { reload: true });
				// 	//$state.go('tab.map');
				// });
   //------------------------------------------------------------
   		$scope.showLoading();
   				//$state.transitionTo('tab.map', {}, { reload: true });

			}, function(reason) {
				// Report status to model
				Reports.submitReportResult(false);

				// Release backdrop
				$ionicLoading.hide();

				// Send error alert
				var errorAlert = $ionicPopup.alert({
					title: 'Error',
					template: 'No se pudo enviar tu reporte, inténtalo más tarde.',
					okType: 'button-energized'
				});

				errorAlert.then(function(res) {
					console.log(res);

				});
			}, function(progress) {
				// Progress report

			});

		});

	};

	$scope.endNewReportProcess = function() {
		// Take us back to the map
		
		$state.go('tab.map');
	};
})