// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('yociudadano', [
  'ionic',
  'ionic.utils',
  'uiGmapgoogle-maps',
  'ngTwitter',
  'youtube-embed',
  'ngIOS9UIWebViewPatch',
  'ngImgur',
  'ionicLazyLoad',
  'ngAnimate',
  'ionic.native',
  'ngCordova'])
.run(function($ionicPlatform, $rootScope, Reports, User) {
  

  $ionicPlatform.ready(function() {
  StatusBar.hide();

          //alert("estas en run");
         window.addEventListener('native.keyboardshow', function(){
        console.log("muestra teclado");
        document.querySelector('div.tabs').style.display = 'none';
        console.log("funciona");
        angular.element(document.querySelector('ion-content.has-tabs')).css('bottom', 0);
        angular.element(document.querySelector('.comment-bar.bar.bar-footer.bar-stable')).css('bottom', 0);
        angular.element(document.querySelector('.has-footer.has-tabs')).css('bottom', 0);
        });

        window.addEventListener('native.keyboardhide', function () {
          console.log("quita teclado");
        var tabs = document.querySelectorAll('div.tabs');
        angular.element(tabs[0]).css('display', '');
        angular.element(document.querySelector('.comment-bar.bar.bar-footer.bar-stable')).css('bottom', 50);
        angular.element(document.querySelector('.has-footer.has-tabs')).css('bottom', 93);
        });





        // window.plugins.nativepagetransitions.globalOptions.duration = 500;
        // window.plugins.nativepagetransitions.globalOptions.iosdelay = 350;
        // window.plugins.nativepagetransitions.globalOptions.androiddelay = 350;
        // window.plugins.nativepagetransitions.globalOptions.winphonedelay = 350;
        // window.plugins.nativepagetransitions.globalOptions.slowdownfactor = 4;
        // // these are used for slide left/right only currently
        // window.plugins.nativepagetransitions.globalOptions.fixedPixelsTop = 0;
        // window.plugins.nativepagetransitions.globalOptions.fixedPixelsBottom = 0;
    
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      cordova.plugins.Keyboard.disableScroll(false);
    }
    if(window.StatusBar) {
      StatusBar.styleLightContent();
    }

  });
//-------------------------------
})

.config(function($ionicConfigProvider) {
    $ionicConfigProvider.tabs.style('standard').position('bottom');
    $ionicConfigProvider.navBar.alignTitle('center');
    $ionicConfigProvider.backButton.text('').icon('icon-yc-back').previousTitleText(false);
    $ionicConfigProvider.scrolling.jsScrolling(false);
    $ionicConfigProvider.views.maxCache(2);

})

.config(function ($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist(['self', new RegExp('^(http[s]?):\/\/(w{3}.)?youtube\.com/.+$')]);
})


.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html',
    controller: 'TabsCtrl',
    // Do not load the state until User has been populated
    resolve: {
      populateSession: function(User) {
        return User.checkSession();
      }
    },
    onEnter: function($state, User, $window) 
    {
      User.checkSession().then(function(hasSession) {
        if (!hasSession) $window.location.href = 'index.html'; // $state.go('splash');
        // use this so that views aren't cached incomplete and can load successfully on
        // next login without refresh.
      });
    }
  })

  // Each tab hast its own nav history stack
  .state('tab.map', {
    url: '/map',
    cache: false,
    views: {
      'tab-map': {
        templateUrl: 'templates/map.html',
        controller: 'MapCtrl'
      }
    },
    onEnter: function($ionicPlatform, Reports, User) {
      $ionicPlatform.ready(function() {
        if(window.StatusBar) {
          StatusBar.hide();
        }
      });
    }
  })
// Mi Actividad-------------------------------
  .state('tab.activity', {
    url: '/activity',
    views: {
      'tab-activity': {
        templateUrl: 'templates/activity.html',
        controller: 'ActivityCtrl'
      }
    },
    onEnter: function($ionicPlatform) {
      $ionicPlatform.ready(function() {
        if(window.StatusBar) {
          StatusBar.show();
        }
      });
    }
  })
// Directorio-----------------------------------
  .state('tab.contacts', {
    url: '/contacts',
    views: {
      'tab-contacts': {
        templateUrl: 'templates/contacts.html',
        controller: 'ContactsCtrl'
      }
    },
    onEnter: function($ionicPlatform) {
      $ionicPlatform.ready(function() {
        if(window.StatusBar) {
          StatusBar.show();
        }
      });
    }
  })
// YoCTV-------------------------------------
  .state('tab.yctv', {
    url: '/yctv',
    views: {
      'tab-yctv': {
        templateUrl: 'templates/yctv.html',
        controller: 'YctvCtrl'
      }
    },
    onEnter: function($ionicPlatform, Video) {
      $ionicPlatform.ready(function() {
        if(window.StatusBar) {
          StatusBar.show();
        }



      });
    }
  })
 //--------------------Otra Cosa
   .state('tab.otracosa', {
    url: '/otracosa',
    views: {
      'tab-otracosa': {
        templateUrl: 'templates/otracosa.html',
        controller: 'OtracosaCtrl'
      }
    },
    onEnter: function($ionicPlatform) {
      $ionicPlatform.ready(function() {
        if(window.StatusBar) {
          StatusBar.show();
        }
      });
    }
  })
   

//--------------------Profile---------------------
  .state('tab.profile', {
    url: '/profile',
    views: {
      'tab-profile': {
        templateUrl: 'templates/profile.html',
        controller: 'PerfilCtrl'
      }
    },
    onEnter: function($ionicPlatform) {
      $ionicPlatform.ready(function() {
        if(window.StatusBar) {
          StatusBar.show();
        }
      });
    }
  })
//-----------------------------------About--------------
  .state('tab.about', {
    url: '/about',
    views: {
      'tab-profile': {
        templateUrl: 'templates/about.html',
        controller: 'AboutCtrl'
      }
    },
    onEnter: function($ionicPlatform) {
      $ionicPlatform.ready(function() {
        if(window.StatusBar) {
          StatusBar.show();
        }
      });
    }
  })


  .state('tab.terms', {
    url: '/terms',
    views: {
      'tab-profile': {
        templateUrl: 'templates/terms.html',
        controller: 'AboutCtrl'
      }
    },
    onEnter: function($ionicPlatform) {
      $ionicPlatform.ready(function() {
        if(window.StatusBar) {
          StatusBar.show();
        }
      });
    }
  })



  .state('tab.map_report_detail', {
    url: '/map_report_detail/:id',
    views: {
      'tab-map': {
        templateUrl: 'templates/report_detail.html',
        controller: 'ReportDetailCtrl'
      }
    },
    onEnter: function($ionicPlatform) {
      $ionicPlatform.ready(function() {
        if(window.StatusBar) {
          StatusBar.show();
          //StatusBar.hide(); 
        }
      });
    }
  })

  .state('tab.new_report', {
    url: '/map_new_report',
    cache: false,
    views: {
      'tab-map': {
        templateUrl: 'templates/new_report.html',
        controller: 'NewReportCtrl'
      }
    },
    onEnter: function($ionicPlatform, $window, $state) {
      $ionicPlatform.ready(function() {
         

        if (window.StatusBar) {
          StatusBar.show();
        }
      });
    }
  })

  .state('tab.activity_report_detail', {
    url: '/activity_report_detail/:id',
    views: {
      'tab-activity': {
        templateUrl: 'templates/report_detail.html',
        controller: 'ReportDetailCtrl'
      }
    }
  })

  // Individual views
  .state('splash',  {
    url: '/',
    templateUrl: 'templates/splash.html',
    controller: 'SplashCtrl',
    onEnter: function($state, User) {

      //cordova.plugins.camerapreview.hide();
      User.checkSession().then(function(hasSession) {
        if (hasSession) $state.go('tab.map');
      });
    }
  })

  .state('signup', {
    url: '/signup',
    templateUrl: 'templates/signup.html',
    controller: 'SignupCtrl'
  })
    //STATE INTRO
  .state('intro', {
    url: '/intro',
    templateUrl: 'templates/intro.html',
    controller: 'IntroCtrl'
  })

  .state('finish_signup', {
    url: '/finish_signup',
    templateUrl: 'templates/finish_signup.html',
    controller: 'FinishSignupCtrl'
  })

  //Si se autentifica por twitter
  .state('finish_signupo', {
    url: '/finish_signupo',
    templateUrl: 'templates/finish_twitter.html',
    controller: 'FinishSignupCtrlo'
  })
  // If none of the above states are matched, use this as the fallback:
  $urlRouterProvider.otherwise('/');
})

.config(function (uiGmapGoogleMapApiProvider) {
  uiGmapGoogleMapApiProvider.configure({
    key: 'AIzaSyA47C4qJmfWX1vVgsj0plfElJPuBAnb46Y',
    v: '3.17',
    libraries: 'visualization'
  });
})

.constant('SERVER', {
  url: 'http://159.203.251.164/'
})

.constant('CLIENTS', {
  facebook: '1467523886874545',
  google:   '189378147966-1hjdhfauim5q51tf1ui5sf2slnirab5u.apps.googleusercontent.com',
  twitter: 'Npc5ideB1SzbwHn7492syZtVo',
  twitterSecret: 'GkRjASfNwQilZvE373hfUSqQRIrAmU2nV6ZrG7zAMDZfyW8El9'
  
})

.directive('keyboardHandler', function ($window) {
        return {
        restrict: 'A',
        link: function postLink(scope, element, attrs) {
            angular.element($window).bind('native.keyboardshow', function() {
                element.addClass('tabs-item-hide');
                
            });

            angular.element($window).bind('native.keyboardhide', function() {
                element.removeClass('tabs-item-hide');
               
            });
        }
    };
})

//FACTORY VALIDATOR

.factory("formValidator", function($ionicPopup){

    return {
        faultImage: function(){
          var alertPopup = $ionicPopup.alert({
             title: 'Falta fotografía',
             template: 'Por favor agrega una fotografía ya sea desde tu cámara o desde tu galería de fotos.'
           });

            return alertPopup;
        },
        faultCategory: function(){
      var alertPopup = $ionicPopup.alert({
             title: 'Falta categoria',
             template: 'Por favor elige el tipo de problema antes de enviar tu reporte.'
           });

            return alertPopup;
        },
        faultDetails: function(){
          var alertPopup = $ionicPopup.alert({
             title: 'Faltan Comentarios Adicionales',
             template: 'Por favor agrega información adicional a tu reporte para facilitarnos la ubicación del problema.'
           });

            return alertPopup;

        },faultDatailsmax: function(){
          var alertPopup = $ionicPopup.alert({
             title: 'Descripción muy larga',
             template: 'Por favor agrega una descripcion mas corta, una vez aprobado el reporte puedes ampliar los datos del problema en los comentarios.'
           });

            return alertPopup;
        }
    }

})


//FACTORY

//FACTORY

.factory('serviceMap', function($http, User, $location) {
  var session_id = User.session_id;
  var reporteGlobal = 0;
  var getReportes = function(callbackFn){
   // alert("estas en getReportes");
    //alert(User.session_id);
    var req = {
      method: 'GET',
      url: 'http://159.203.251.164/api/v1/reports',
      headers: {
        'SessionId': User.session_id
      }
    }; 

    //alert("vas a hacer el req");
    $http(req).success(function(data){
      callbackFn(data);
    });

    //return reportes ;
    // return $http(req).then(function(response){
    //   console.log(response);
    //   return alert("exito");
    // }, function(error){
    //   alert("error");
    //   return alert(error);
    // });

  };



  return {
    getPonies: getReportes,
    hideMap: function(){
      return $( ".card").hide();
    },
    vamonos: function()
    {
      $location.url('/tab/map_report_detail/' + reporteGlobal);
    },
    vamonosVaquero: function()
    {
      //hub("eres un vaquero");
       // var agSender;
 //    var pushConfig = {
 //        pushServerURL: "https://jbossunifiedpush1-denialtorres.rhcloud.com/ag-push",
 //        alias: username,
 //        android: {
 //          senderID: "189378147966",
 //          variantID: "a83fdb3b-5cc7-4d4d-a773-4b9c92343d71",
 //          variantSecret: "5b1b572a-564f-4b33-8063-10bcd0848d3f"
 //        },
 //        ios: {
 //          variantID: "f69b3cd9-6ffc-4588-976b-b321535f8086",
 //          variantSecret: "8cf7fbf7-d5f0-4dab-b07e-47f7223c624f"
 //        }
 //    };
 //    var statusList = $("#app-status-ul");
 //    statusList.append('<li>deviceready event received</li>');
 //    try {
 //      statusList.append('<li>registering </li>');
 //      push.register(onNotification, successHandler, errorHandler, pushConfig);
 //    } catch (err) {
 //      txt = "There was an error on this page.\n\n";
 //      txt += "Error description: " + err.message + "\n\n";
 //      alert(txt);
 //    }
 //  // alert('tu nombre de usuario es '+ username); 
 //   var usuario = username;
 //   var admin = "admin"
      // $location.url('/tab/map_report_detail/' + reporteGlobal);
    },
    marker: function(map, reportes){
      console.log("Tienes " + reportes.length + " reportes");
      //console.log(map);
        map.addEventListener(plugin.google.maps.event.MAP_CLICK, function(){
          $( ".card").hide();
        });


       var icons = {
   pavimento:'www/img/marcador_idle_pavimento-original.png',
   alumbrado:'www/img/marcador_idle_alumbrado-original.png',
   parques:'www/img/marcador_idle_parques-original.png',
   limpia:'www/img/marcador_idle_limpia-original.png',
   accesibilidad: 'www/img/marcador_idle_acces-original.png'
      };

 //AQUI ASIGNAS ICONOS A LOS REPORTES.
  angular.forEach(reportes, function(value, key){
   if(value.report_category_id == 2)
   {
     value.icon = icons.pavimento;
   }else if(value.report_category_id == 1){
     value.icon = icons.limpia;
   }else if(value.report_category_id == 3){
     value.icon = icons.parques

   }else if(value.report_category_id == 4){
     value.icon = icons.alumbrado
   }else if(value.report_category_id == 5){
     value.icon = icons.accesibilidad
   }

   });

   console.log(reportes);


   //AQUI VIENE LO CHINGON, ASIGNAS REPORTES

     for(var i = 0; i < reportes.length; i++){
     map.addMarker({
       'index' : i,
       'latitud': reportes[i].latitude,
       'longitude': reportes[i].longitude,
       'image_url': reportes[i].image_url,
       'reporte_id': reportes[i].id,
       'categoria': reportes[i].get_report_category,
       'position': new plugin.google.maps.LatLng(reportes[i].latitude, reportes[i].longitude),
       'address' : reportes[i].address,
       'comentarios' : reportes[i].comments_conversion,
       'me_afecta' : reportes[i].votes_conversion,
       'icon': {
         'url': reportes[i].icon,
         'size':{
           'width': 42,
           'height': 53
         }
       }
     },function(marker, $rootScope, $compile){
      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
       reporte = marker.get('reporte_id'); 
       // $scope.reporte = reporte; 
       //   alert("tu reporte es " + reporte);
          $rootScope.reporte = reporte;
         marker.setAnimation(plugin.google.maps.Animation.BOUNCE);
         marker.hideInfoWindow();
         //alert("diste click en el pinchi marcador");
         //alert(marker.get('categoria'));
         $( ".card").show();
          // console.log(marker.get('image_url')); 
          // $( ".card").show();
          // $( "#valor" ).empty().append( marker.get('index'));
          // $( "#reporte_id" ).empty().append( marker.get('reporte_id'));

          $("#categoria").empty().append("Problema de " + marker.get('categoria'));
           $("#comentarios").empty().append(marker.get('comentarios'));
           $("#me_afecta").empty().append(marker.get('me_afecta'));
          // $( "#direccion" ).empty().append( marker.get('address') );
      $( "#direccion" ).empty().append( marker.get('address') ).ellipsis({ lines: 2 ,
        responsive: true});
          var img = document.createElement("IMG");
          img.src = marker.get('image_url');
          $('#image').html(img); 
          reporteGlobal = reporte;
          //$('#image').prepend($('<img>',{id:'theImg',src:'theImg.png'}))
       });
   })

 } //<==== CICLO FOR
    

    },
    putMarker: function(valores, $scope){
      //alert("aqui tienes todo para crear los marcadores, depende de ti");
      //console.log($scope.map);
      console.log(valores);
      return alert(valores.length);
    },
    reportesUsuario: function(reportes){
      var usuario = [];
      
      if(User.user_id == false)
        {console.log("hubo un error para obtener tus reportes, por favor sal de la sesion e inicia nuevamente");
        
        }

      angular.forEach(reportes, function(value,key){
        if(value.user_id == User.user_id){
          console.log("ESTE REPORTE ES TUYO");
          usuario.push(value);
        }   
      });

      return usuario;

    },
    mamada: function(data){
    var array = [];  
    angular.forEach(data, function(value, key) {
        value.image_url = 'http://159.203.251.164/' + value.image_url;
        var value1 = value.image_url;
        var res = value1.replace("/original/", "/medium/");
        value.image_url = res; 
        array.push(value);
      });
      return array;
    },
    createMap: function(){
    var initPosition = new plugin.google.maps.LatLng(-23.548, -46.5745);
    var div = document.getElementById("map_canvas");
      const GORYOKAKU_JAPAN = new plugin.google.maps.LatLng(31.739444444444,-106.48694444444);
      if(plugin.google)
    var map = plugin.google.maps.Map.getMap(div,{
      'backgroundColor': 'white',
      'mapType': plugin.google.maps.MapTypeId.ROADMAP,
      'controls': {
      'compass': false,
      'myLocationButton': true,
      'indoorPicker': true,
      'zoom': true
       },
      'gestures': {
      'scroll': true,
      'tilt': true,
      'rotate': true,
      'zoom' : true
       },
      'camera': {
      'latLng': new plugin.google.maps.LatLng(31.739444444444,-106.48694444444),
      'tilt': 0,
      'zoom': 10,
      'bearing': 0
       }
       });




      return map;
    },
      markerTodos: function(map, reportes){
      console.log("Tienes " + reportes.length + " reportes");
        map.addEventListener(plugin.google.maps.event.MAP_CLICK, function(){
          $( ".card").hide();
        });
       var icons = {
   pavimento:'www/img/marcador_idle_pavimento-original.png',
   alumbrado:'www/img/marcador_idle_alumbrado-original.png',
   parques:'www/img/marcador_idle_parques-original.png',
   limpia:'www/img/marcador_idle_limpia-original.png',
   accesibilidad: 'www/img/marcador_idle_acces-original.png'
      };

 //AQUI ASIGNAS ICONOS A LOS REPORTES.
  angular.forEach(reportes, function(value, key){
   if(value.report_category_id == 2)
   {
     value.icon = icons.pavimento;
   }else if(value.report_category_id == 1){
     value.icon = icons.limpia;
   }else if(value.report_category_id == 3){
     value.icon = icons.parques

   }else if(value.report_category_id == 4){
     value.icon = icons.alumbrado
   }else if(value.report_category_id == 5){
     value.icon = icons.accesibilidad
   }

   });

   console.log(reportes);


   //AQUI VIENE LO CHINGON, ASIGNAS REPORTES

     for(var i = 0; i < reportes.length; i++){
     map.addMarker({
       'index' : i,
       'latitud': reportes[i].latitude,
       'longitude': reportes[i].longitude,
       'image_url': reportes[i].image_url,
       'reporte_id': reportes[i].id,
       'categoria': reportes[i].get_report_category,
       'position': new plugin.google.maps.LatLng(reportes[i].latitude, reportes[i].longitude),
       'address' : reportes[i].address,
       'comentarios' : reportes[i].comments_conversion,
       'me_afecta' : reportes[i].votes_conversion,
       'icon': {
         'url': reportes[i].icon,
         'size':{
           'width': 42,
           'height': 53
         }
       }
     },function(marker, $rootScope, $compile){
      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
       reporte = marker.get('reporte_id'); 
       // $scope.reporte = reporte; 
       //   alert("tu reporte es " + reporte);
          $rootScope.reporte = reporte;
         marker.setAnimation(plugin.google.maps.Animation.BOUNCE);
         marker.hideInfoWindow();
         //alert("diste click en el pinchi marcador");
         //alert(marker.get('categoria'));
         $( ".card").show();
          // console.log(marker.get('image_url')); 
          // $( ".card").show();
          // $( "#valor" ).empty().append( marker.get('index'));
          // $( "#reporte_id" ).empty().append( marker.get('reporte_id'));

          $("#categoria").empty().append("Problema de " + marker.get('categoria'));
           $("#comentarios").empty().append(marker.get('comentarios'));
           $("#me_afecta").empty().append(marker.get('me_afecta'));
          // $( "#direccion" ).empty().append( marker.get('address') );
      $( "#direccion" ).empty().append( marker.get('address') ).ellipsis({ lines: 2 ,
        responsive: true});
          var img = document.createElement("IMG");
          img.src = marker.get('image_url');
          $('#image').html(img); 
          reporteGlobal = reporte;
          //$('#image').prepend($('<img>',{id:'theImg',src:'theImg.png'}))
       });
   })

 } //<==== CICLO FOR


    },
      markerPavimento: function(map, reportes){
      console.log("Tienes " + reportes.length + " reportes");
        map.addEventListener(plugin.google.maps.event.MAP_CLICK, function(){
          $( ".card").hide();
        });
       var icons = {
   pavimento:'www/img/marcador_idle_pavimento-original.png',
   alumbrado:'www/img/marcador_idle_alumbrado-original.png',
   parques:'www/img/marcador_idle_parques-original.png',
   limpia:'www/img/marcador_idle_limpia-original.png'
      };

 //AQUI ASIGNAS ICONOS A LOS REPORTES.
  angular.forEach(reportes, function(value, key){
   if(value.report_category_id == 2)
   {
     value.icon = icons.pavimento;
   }else if(value.report_category_id == 1){
     value.icon = icons.limpia;
   }else if(value.report_category_id == 3){
     value.icon = icons.parques

   }else if(value.report_category_id == 4){
     value.icon = icons.alumbrado
   }

   });

   console.log(reportes);


   //AQUI VIENE LO CHINGON, ASIGNAS REPORTES

     for(var i = 0; i < reportes.length; i++){

      if(reportes[i].get_report_category == 'Pavimento'){
               map.addMarker({
       'index' : i,
       'latitud': reportes[i].latitude,
       'longitude': reportes[i].longitude,
       'image_url': reportes[i].image_url,
       'reporte_id': reportes[i].id,
       'categoria': reportes[i].get_report_category,
       'position': new plugin.google.maps.LatLng(reportes[i].latitude, reportes[i].longitude),
       'address' : reportes[i].address,
       'comentarios' : reportes[i].comments_conversion,
       'me_afecta' : reportes[i].votes_conversion,
       'icon': {
         'url': reportes[i].icon,
         'size':{
           'width': 42,
           'height': 53
         }
       }
     },function(marker, $rootScope, $compile){
      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
       reporte = marker.get('reporte_id'); 
       // $scope.reporte = reporte; 
       //   alert("tu reporte es " + reporte);
          $rootScope.reporte = reporte;
         marker.setAnimation(plugin.google.maps.Animation.BOUNCE);
         marker.hideInfoWindow();
         //alert("diste click en el pinchi marcador");
         //alert(marker.get('categoria'));
         $( ".card").show();
          // console.log(marker.get('image_url')); 
          // $( ".card").show();
          // $( "#valor" ).empty().append( marker.get('index'));
          // $( "#reporte_id" ).empty().append( marker.get('reporte_id'));

          $("#categoria").empty().append("Problema de " + marker.get('categoria'));
           $("#comentarios").empty().append(marker.get('comentarios'));
           $("#me_afecta").empty().append(marker.get('me_afecta'));
          // $( "#direccion" ).empty().append( marker.get('address') );
      $( "#direccion" ).empty().append( marker.get('address') ).ellipsis({ lines: 2 ,
        responsive: true});
          var img = document.createElement("IMG");
          img.src = marker.get('image_url');
          $('#image').html(img); 
          reporteGlobal = reporte;
          //$('#image').prepend($('<img>',{id:'theImg',src:'theImg.png'}))
       });
   })

      }

 } //<==== CICLO FOR


    },
      markerAlumbrado: function(map, reportes){
      console.log("Tienes " + reportes.length + " reportes");
        map.addEventListener(plugin.google.maps.event.MAP_CLICK, function(){
          $( ".card").hide();
        });
       var icons = {
   pavimento:'www/img/marcador_idle_pavimento-original.png',
   alumbrado:'www/img/marcador_idle_alumbrado-original.png',
   parques:'www/img/marcador_idle_parques-original.png',
   limpia:'www/img/marcador_idle_limpia-original.png'
      };

 //AQUI ASIGNAS ICONOS A LOS REPORTES.
  angular.forEach(reportes, function(value, key){
   if(value.report_category_id == 2)
   {
     value.icon = icons.pavimento;
   }else if(value.report_category_id == 1){
     value.icon = icons.limpia;
   }else if(value.report_category_id == 3){
     value.icon = icons.parques

   }else if(value.report_category_id == 4){
     value.icon = icons.alumbrado
   }

   });

   console.log(reportes);


   //AQUI VIENE LO CHINGON, ASIGNAS REPORTES

     for(var i = 0; i < reportes.length; i++){

      if(reportes[i].get_report_category == 'Alumbrado'){
               map.addMarker({
       'index' : i,
       'latitud': reportes[i].latitude,
       'longitude': reportes[i].longitude,
       'image_url': reportes[i].image_url,
       'reporte_id': reportes[i].id,
       'categoria': reportes[i].get_report_category,
       'position': new plugin.google.maps.LatLng(reportes[i].latitude, reportes[i].longitude),
       'address' : reportes[i].address,
       'comentarios' : reportes[i].comments_conversion,
       'me_afecta' : reportes[i].votes_conversion,
       'icon': {
         'url': reportes[i].icon,
         'size':{
           'width': 42,
           'height': 53
         }
       }
     },function(marker, $rootScope, $compile){
      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
       reporte = marker.get('reporte_id'); 
       // $scope.reporte = reporte; 
       //   alert("tu reporte es " + reporte);
          $rootScope.reporte = reporte;
         marker.setAnimation(plugin.google.maps.Animation.BOUNCE);
         marker.hideInfoWindow();
         //alert("diste click en el pinchi marcador");
         //alert(marker.get('categoria'));
         $( ".card").show();
          // console.log(marker.get('image_url')); 
          // $( ".card").show();
          // $( "#valor" ).empty().append( marker.get('index'));
          // $( "#reporte_id" ).empty().append( marker.get('reporte_id'));

          $("#categoria").empty().append("Problema de " + marker.get('categoria'));
           $("#comentarios").empty().append(marker.get('comentarios'));
           $("#me_afecta").empty().append(marker.get('me_afecta'));
          // $( "#direccion" ).empty().append( marker.get('address') );
      $( "#direccion" ).empty().append( marker.get('address') ).ellipsis({ lines: 2 ,
        responsive: true});
          var img = document.createElement("IMG");
          img.src = marker.get('image_url');
          $('#image').html(img); 
          reporteGlobal = reporte;
          //$('#image').prepend($('<img>',{id:'theImg',src:'theImg.png'}))
       });
   })

      }

 } //<==== CICLO FOR


    },
      markerParques: function(map, reportes){
      console.log("Tienes " + reportes.length + " reportes");
        map.addEventListener(plugin.google.maps.event.MAP_CLICK, function(){
          $( ".card").hide();
        });
       var icons = {
   pavimento:'www/img/marcador_idle_pavimento-original.png',
   alumbrado:'www/img/marcador_idle_alumbrado-original.png',
   parques:'www/img/marcador_idle_parques-original.png',
   limpia:'www/img/marcador_idle_limpia-original.png'
      };

 //AQUI ASIGNAS ICONOS A LOS REPORTES.
  angular.forEach(reportes, function(value, key){
   if(value.report_category_id == 2)
   {
     value.icon = icons.pavimento;
   }else if(value.report_category_id == 1){
     value.icon = icons.limpia;
   }else if(value.report_category_id == 3){
     value.icon = icons.parques

   }else if(value.report_category_id == 4){
     value.icon = icons.alumbrado
   }

   });

   console.log(reportes);


   //AQUI VIENE LO CHINGON, ASIGNAS REPORTES

     for(var i = 0; i < reportes.length; i++){

      if(reportes[i].get_report_category == 'Parques'){
               map.addMarker({
       'index' : i,
       'latitud': reportes[i].latitude,
       'longitude': reportes[i].longitude,
       'image_url': reportes[i].image_url,
       'reporte_id': reportes[i].id,
       'categoria': reportes[i].get_report_category,
       'position': new plugin.google.maps.LatLng(reportes[i].latitude, reportes[i].longitude),
       'address' : reportes[i].address,
       'comentarios' : reportes[i].comments_conversion,
       'me_afecta' : reportes[i].votes_conversion,
       'icon': {
         'url': reportes[i].icon,
         'size':{
           'width': 42,
           'height': 53
         }
       }
     },function(marker, $rootScope, $compile){
      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
       reporte = marker.get('reporte_id'); 
       // $scope.reporte = reporte; 
       //   alert("tu reporte es " + reporte);
          $rootScope.reporte = reporte;
         marker.setAnimation(plugin.google.maps.Animation.BOUNCE);
         marker.hideInfoWindow();
         //alert("diste click en el pinchi marcador");
         //alert(marker.get('categoria'));
         $( ".card").show();
          // console.log(marker.get('image_url')); 
          // $( ".card").show();
          // $( "#valor" ).empty().append( marker.get('index'));
          // $( "#reporte_id" ).empty().append( marker.get('reporte_id'));

          $("#categoria").empty().append("Problema de " + marker.get('categoria'));
           $("#comentarios").empty().append(marker.get('comentarios'));
           $("#me_afecta").empty().append(marker.get('me_afecta'));
          // $( "#direccion" ).empty().append( marker.get('address') );
      $( "#direccion" ).empty().append( marker.get('address') ).ellipsis({ lines: 2 ,
        responsive: true});
          var img = document.createElement("IMG");
          img.src = marker.get('image_url');
          $('#image').html(img); 
          reporteGlobal = reporte;
          //$('#image').prepend($('<img>',{id:'theImg',src:'theImg.png'}))
       });
   })

      }

 } //<==== CICLO FOR


    },
      markerLimpia: function(map, reportes){
      console.log("Tienes " + reportes.length + " reportes");
        map.addEventListener(plugin.google.maps.event.MAP_CLICK, function(){
          $( ".card").hide();
        });
       var icons = {
   pavimento:'www/img/marcador_idle_pavimento-original.png',
   alumbrado:'www/img/marcador_idle_alumbrado-original.png',
   parques:'www/img/marcador_idle_parques-original.png',
   limpia:'www/img/marcador_idle_limpia-original.png'
      };

 //AQUI ASIGNAS ICONOS A LOS REPORTES.
  angular.forEach(reportes, function(value, key){
   if(value.report_category_id == 2)
   {
     value.icon = icons.pavimento;
   }else if(value.report_category_id == 1){
     value.icon = icons.limpia;
   }else if(value.report_category_id == 3){
     value.icon = icons.parques

   }else if(value.report_category_id == 4){
     value.icon = icons.alumbrado
   }

   });

   console.log(reportes);


   //AQUI VIENE LO CHINGON, ASIGNAS REPORTES

     for(var i = 0; i < reportes.length; i++){

      if(reportes[i].get_report_category == 'Limpia'){
               map.addMarker({
       'index' : i,
       'latitud': reportes[i].latitude,
       'longitude': reportes[i].longitude,
       'image_url': reportes[i].image_url,
       'reporte_id': reportes[i].id,
       'categoria': reportes[i].get_report_category,
       'position': new plugin.google.maps.LatLng(reportes[i].latitude, reportes[i].longitude),
       'address' : reportes[i].address,
       'comentarios' : reportes[i].comments_conversion,
       'me_afecta' : reportes[i].votes_conversion,
       'icon': {
         'url': reportes[i].icon,
         'size':{
           'width': 42,
           'height': 53
         }
       }
     },function(marker, $rootScope, $compile){
      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
       reporte = marker.get('reporte_id'); 
       // $scope.reporte = reporte; 
       //   alert("tu reporte es " + reporte);
          $rootScope.reporte = reporte;
         marker.setAnimation(plugin.google.maps.Animation.BOUNCE);
         marker.hideInfoWindow();
         //alert("diste click en el pinchi marcador");
         //alert(marker.get('categoria'));
         $( ".card").show();
          // console.log(marker.get('image_url')); 
          // $( ".card").show();
          // $( "#valor" ).empty().append( marker.get('index'));
          // $( "#reporte_id" ).empty().append( marker.get('reporte_id'));

          $("#categoria").empty().append("Problema de " + marker.get('categoria'));
           $("#comentarios").empty().append(marker.get('comentarios'));
           $("#me_afecta").empty().append(marker.get('me_afecta'));
          // $( "#direccion" ).empty().append( marker.get('address') );
      $( "#direccion" ).empty().append( marker.get('address') ).ellipsis({ lines: 2 ,
        responsive: true});
          var img = document.createElement("IMG");
          img.src = marker.get('image_url');
          $('#image').html(img); 
          reporteGlobal = reporte;
          //$('#image').prepend($('<img>',{id:'theImg',src:'theImg.png'}))
       });
   })

      }

 } //<==== CICLO FOR


    },
      markerAccesibilidad: function(map, reportes){
      console.log("Tienes " + reportes.length + " reportes");
        map.addEventListener(plugin.google.maps.event.MAP_CLICK, function(){
          $( ".card").hide();
        });
       var icons = {
   pavimento:'www/img/marcador_idle_pavimento-original.png',
   alumbrado:'www/img/marcador_idle_alumbrado-original.png',
   parques:'www/img/marcador_idle_parques-original.png',
   limpia:'www/img/marcador_idle_limpia-original.png'
      };

 //AQUI ASIGNAS ICONOS A LOS REPORTES.
  angular.forEach(reportes, function(value, key){
   if(value.report_category_id == 2)
   {
     value.icon = icons.pavimento;
   }else if(value.report_category_id == 1){
     value.icon = icons.limpia;
   }else if(value.report_category_id == 3){
     value.icon = icons.parques

   }else if(value.report_category_id == 4){
     value.icon = icons.alumbrado
   }

   });

   console.log(reportes);


   //AQUI VIENE LO CHINGON, ASIGNAS REPORTES

     for(var i = 0; i < reportes.length; i++){

      if(reportes[i].get_report_category == 'Accesibilidad'){
               map.addMarker({
       'index' : i,
       'latitud': reportes[i].latitude,
       'longitude': reportes[i].longitude,
       'image_url': reportes[i].image_url,
       'reporte_id': reportes[i].id,
       'categoria': reportes[i].get_report_category,
       'position': new plugin.google.maps.LatLng(reportes[i].latitude, reportes[i].longitude),
       'address' : reportes[i].address,
       'comentarios' : reportes[i].comments_conversion,
       'me_afecta' : reportes[i].votes_conversion,
       'icon': {
         'url': reportes[i].icon,
         'size':{
           'width': 42,
           'height': 53
         }
       }
     },function(marker, $rootScope, $compile){
      marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
       reporte = marker.get('reporte_id'); 
       // $scope.reporte = reporte; 
       //   alert("tu reporte es " + reporte);
          $rootScope.reporte = reporte;
         marker.setAnimation(plugin.google.maps.Animation.BOUNCE);
         marker.hideInfoWindow();
         //alert("diste click en el pinchi marcador");
         //alert(marker.get('categoria'));
         $( ".card").show();
          // console.log(marker.get('image_url')); 
          // $( ".card").show();
          // $( "#valor" ).empty().append( marker.get('index'));
          // $( "#reporte_id" ).empty().append( marker.get('reporte_id'));

          $("#categoria").empty().append("Problema de " + marker.get('categoria'));
           $("#comentarios").empty().append(marker.get('comentarios'));
           $("#me_afecta").empty().append(marker.get('me_afecta'));
          // $( "#direccion" ).empty().append( marker.get('address') );
      $( "#direccion" ).empty().append( marker.get('address') ).ellipsis({ lines: 2 ,
        responsive: true});
          var img = document.createElement("IMG");
          img.src = marker.get('image_url');
          $('#image').html(img); 
          reporteGlobal = reporte;
          //$('#image').prepend($('<img>',{id:'theImg',src:'theImg.png'}))
       });
   })

      }

 } //<==== CICLO FOR


    }
  }
})

