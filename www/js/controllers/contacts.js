angular.module('yociudadano')

.controller('ContactsCtrl', function($scope, Contacts, $rootScope, User) {
	$scope.$on('$ionicView.beforeEnter'), function(){
		appointmentsService.getAllAppointments(); 

	};
	$rootScope.bandera = 1; 

	Contacts.init(User.session_id).then(function() {
		$scope.contact_list = Contacts.list;
	});

	$scope.doRefresh = function() {
		Contacts.refresh(User.session_id).then(function() {
			$scope.contact_list = Contacts.list;
		}, function(reason) {
			// Failed to load contacts
			// Send alert error
		}).finally(function() {
			$scope.$broadcast('scroll.refreshComplete');
		});
	}
})