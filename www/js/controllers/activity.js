angular.module('yociudadano')

.controller('ActivityCtrl', function($scope, $rootScope, $location, User, Notifications) {

	Notifications.getNotifications1(User.session_id).then(function() {
		$scope.notifications = Notifications.list;


	}, function(reason) {
		// Send alert error
	});


	// $rootScope.markers = [];
	// $scope.map.markers = [];
	// $rootScope.bandera = 1; 
 	$scope.numero = 0;

	$rootScope.doRefresh = function() {
		$rootScope.badgeNotification(0);
		Notifications.refresh(User.session_id).then(function() {
			$scope.notifications = Notifications.list;
		 	//console.log(Notifications.list);
		 	console.log(Notifications);
		 	
		 	var i = 0;
		 	for(; i< Notifications.list.length; i++){
		 		Notifications.list[i].is_read = true;
		 		console.log(User.session_id, Notifications.list[i].id ) 
		 		Notifications.markNotificationAsRead(User.session_id, Notifications.list[i].id)
		 	}

		}, function(reason) {
			// Send alert error
		}).finally(function() {
			$rootScope.badgeToZero(); 
			$scope.$broadcast('scroll.refreshComplete');
		});
	}

	$scope.notificationIsOrange = function(ar) {
		if (ar.activity_type == 2 || ar.activity_type == 7) {
			return true;
		}
	};
	$scope.notificationIsGreen = function(ar) {
		if (ar.activity_type == 5) {
			return true;
		}
	};
	$scope.notificationIsUnread = function(ar) {

		if (!(ar.activity_type == 2 || ar.activity_type == 5 || ar.activity_type == 7) && ar.is_read == false) {
	
			return true;
		}
	};

	$scope.goToReportDetail = function(ar) {
		$rootScope.medalla = $rootScope.medalla-1
		$rootScope.badgeNotification($rootScope.medalla);	
		console.log(ar.id, User.session_id);
		Notifications.markNotificationAsRead(User.session_id, ar.id).then(function() {
			ar.is_read = true;
			var medalla = 
			$location.url('/tab/activity_report_detail/' + ar.report_id);
		}, function(reason) {
			// Send alert error
		});
	}

	$rootScope.notificador = function() {
		//alert("estas en notificador")
		// $scope.numero++;
		// console.log("esto vale el badge " + $scope.numero);
		// //alert("esto vale el badge " + $scope.numero);
		// return $scope.numero;
	}; 
})