angular.module('yociudadano')
/*
 * Controller for signup
 */
.controller('SignupCtrl', function($scope, $ionicModal, User, $state, $ionicPopup) {
	$scope.days = [];
	$scope.years = [];
	var current_year = new Date().getFullYear();
	var first_year = current_year - 80;
	$scope.months = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
	for (var i=1; i < 32; i++) {
		$scope.days.push(i);
	}
	for (var i=current_year; i > first_year; i--) {
		$scope.years.push(i);
	}

	$scope.signup = function(data) {
		/* 
		 * Validate form before manipulating and sending data
		 * Check for equal password and confirmation
		 * Check for birthdate, name, username, email format.
		 */

		 if(!data) data = {}

		var formIsValid = true;
		var messages = '';
		var emailre = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;

		if(data.password != data.passwordConfirmation) {
			formIsValid = false;
			messages = 'Las contraseñas no son iguales.</br>';
		}

		if(!data.month || !data.day || !data.year || !data.username || !data.email) {
			formIsValid = false;
			messages += 'Favor de llenar todos los campos.</br>';
		}

		if(data.email) {
			if(!emailre.test(data.email))  {
				formIsValid = false;
				messages += 'El formato del correo no es correcto.</br>';
			}
		}

		if(formIsValid) {
			if (data.genderBool == true && data.genderBoolfemenina == false) {
				data.gender = 'M';
			} else {
				data.gender = 'F';
			}
			var month = $scope.months.indexOf(data.month) + 1;
			data.birthdate = data.day + '-' + month + '-' + data.year;
			User.auth(data.username, data.password, true, data).then(function() {
				$ionicPopup.alert({
						title: '<b>Ya casi!</b>',
						template: 'Confirma el correo que te acabamos de enviar para completar el registro, Sino lo haces no podras accesar a la pagina www.yoreporto.com.mx, Saludos!',
						okText: 'Aceptar',
						okType: 'button-energized'
				})
				//$state.go('tab.map');
				$state.go('intro');
			}, function(reason) {
				// reason.data.messages
				var fullErrorMessage = '';
				angular.forEach(reason.data.messages, function(value, index) {
					fullErrorMessage += value + '';
				});
				var alertPopup = $ionicPopup.alert({
					title: '<b>¡Error!</b>',
					template: '<small>' + fullErrorMessage + '</small>',
					okText: 'Aceptar',
					okType: 'button-energized'
				});
				alertPopup.then(function(res) {
					console.log('Popup closed.');
				});
			});
		} else {
			var fullErrorMessage = '';
			angular.forEach(messages, function(value, index) {
				fullErrorMessage += value + '';
			});
			var alertPopup = $ionicPopup.alert({
				title: '<b>¡Error!</b>',
				template: '<small>' + fullErrorMessage + '</small>',
				okText: 'Aceptar',
				okType: 'button-energized'
			});
			alertPopup.then(function(res) {
				console.log('Popup closed.');
			});
		}
	}

	$scope.fbSignup = function() {
		User.fbSignIn().then(function() {
			User.fbGetData().then(function() {
				User.fbAuth().then(function() {
					// Detect if it is a sign in or sign up
					if (User.username) {
						console.log('Controller reports successfull social login.');
						$state.go('tab.map');
					} else {
						// Open finish signup modal
						console.log('Contorller reports this is a new user');
						$state.go('finish_signup');
					}
				}, function() {
					alert('Try again...!');
				});
			}, function() {
				// alert('Could not get your Facebook data...');
			});
		}, function() {
			// alert('Could not sign you into Facebook...');
		});
	}

	$ionicModal.fromTemplateUrl('templates/terms_conditions_modal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.termsModal = modal;
	});

	$scope.openTermsModal = function() {
		$scope.termsModal.show();
	}

	$scope.closeTermsModal = function() {
		$scope.termsModal.hide();
	}
})