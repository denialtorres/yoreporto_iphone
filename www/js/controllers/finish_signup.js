angular.module('yociudadano')

.controller('FinishSignupCtrl', function($scope, $ionicModal, User, $state,  $ionicPopup, $ionicPlatform) {
	$scope.days = [];
	$scope.years = [];
	$ionicPlatform.registerBackButtonAction(function (event) {
                    event.preventDefault();
            }, 100);
	var current_year = new Date().getFullYear();
	var first_year = current_year - 80;
	$scope.months = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
	for (var i=1; i < 32; i++) {
		$scope.days.push(i);
	}
	for (var i=current_year; i > first_year; i--) {
		$scope.years.push(i);
	}

	$scope.finishSignup = function(data) {
		/* 
		 * Validate form before manipulating and sending data
		 * Check for equal password and confirmation
		 * Check for birthdate, name, username, email format.
		 */

		if(!data) data = {}

		var formIsValid = true;
		var messages = '';

		if(!data.month || !data.day || !data.year || !data.username) {
			formIsValid = false;
			messages += 'Favor de llenar todos los campos.</br>';
		}

		if(formIsValid) {
			if (data.genderBool) {
				data.gender = 'F';
			} else {
				data.gender = 'M';
			}
			var month = $scope.months.indexOf(data.month) + 1;
			data.birthdate = data.day + '-' + month + '-' + data.year;
			//alert("estos datos vas a mandar" + JSON.stringify(data));
			User.finishSignup(data).then(function() {
				//$state.go('tab.map');
				$state.go('intro');
			}, function(reason) {
				// reason.data.messages
				var fullErrorMessage = '';
				angular.forEach(reason.data.messages, function(value, index) {
					fullErrorMessage += value + '';
				});
				var alertPopup = $ionicPopup.alert({
					title: '<b>¡Error!</b>',
					template: '<small>' + fullErrorMessage + '</small>',
					okText: 'Aceptar',
					okType: 'button-energized'
				});
				alertPopup.then(function(res) {
					console.log('Popup closed.');
				});
			});
		} else {
			var fullErrorMessage = '';
			angular.forEach(messages, function(value, index) {
				fullErrorMessage += value + '</br>';
			});
			 $ionicPopup.alert({
						title: '<b>Datos incompletos!!</b>',
						template: 'Por favor llena todos los datos que te pida el registro, tus datos personales estan protegidos',
						okText: 'Aceptar',
						okType: 'button-energized'
					})
			alertPopup.then(function(res) {
				console.log('Popup closed.');
			});
		}
	}

	$ionicModal.fromTemplateUrl('templates/terms_conditions_modal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.termsModal = modal;
	});

	$scope.openTermsModal = function() {
		$scope.termsModal.show();
	}

	$scope.closeTermsModal = function() {
		$scope.termsModal.hide();
	}
})