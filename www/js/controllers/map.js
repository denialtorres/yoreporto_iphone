	angular.module('yociudadano')

.controller('MapCtrl', function(
	serviceMap,
	$scope,
	$rootScope,
	$state,
	$ionicHistory,
	$ionicPopup,
	$ionicLoading,
	$ionicActionSheet,
	$timeout,
	$ionicPlatform,
	$cordovaSocialSharing,
	uiGmapIsReady,
	uiGmapGoogleMapApi,
	Geolocation,
	Reports,
	User,
	Notifications,
	$window,
	$location,
	$ionicSideMenuDelegate
) {


	//$scope.reporte = null; 
	$ionicSideMenuDelegate.canDragContent(false)

$scope.$on('$ionicView.enter', function(){
  // Anything you can think of
  $scope.map = serviceMap.createMap();


  $rootScope.misReportes = function(){
      $scope.map.clear();
      $scope.map.off();
      serviceMap.markerTodos($scope.map, $scope.reportesUsuario);
      $("#menu-list-item1").css("background-color", "rgba(255, 255, 255, 0.4)");
      $("#menu-list-item2").css("background-color", "transparent");
      $("#menu-list-item3").css("background-color", "transparent");
      $("#menu-list-item7").css("background-color", "transparent");
      $("#menu-list-item8").css("background-color", "transparent");
      $("#menu-list-item9").css("background-color", "transparent");
      $("#menu-list-item10").css("background-color", "transparent");
    
    };


  $rootScope.todosReportes = function(){
    $scope.map.clear();
    $scope.map.off();
    serviceMap.markerTodos($scope.map, $scope.reportes);
      $("#menu-list-item1").css("background-color", "transparent");
      $("#menu-list-item2").css("background-color", "rgba(255, 255, 255, 0.4)");
      $("#menu-list-item3").css("background-color", "transparent");
      $("#menu-list-item7").css("background-color", "transparent");
      $("#menu-list-item8").css("background-color", "transparent");
      $("#menu-list-item9").css("background-color", "transparent");
      $("#menu-list-item10").css("background-color", "transparent");

    };



  $rootScope.soloPavimento = function(){
    console.log($scope.map);
    $scope.map.clear();
    $scope.map.off();
    console.log($scope.reportes.length);
    serviceMap.markerPavimento($scope.map, $scope.reportes);
      $("#menu-list-item1").css("background-color", "transparent");
      $("#menu-list-item2").css("background-color", "transparent");
      $("#menu-list-item3").css("background-color", "rgba(255, 255, 255, 0.4)");
      $("#menu-list-item7").css("background-color", "transparent");
      $("#menu-list-item8").css("background-color", "transparent");
      $("#menu-list-item9").css("background-color", "transparent");
      $("#menu-list-item10").css("background-color", "transparent");
   }


    $rootScope.soloAlumbrado = function(){
       $scope.map.clear();
    $scope.map.off();
    serviceMap.markerAlumbrado($scope.map, $scope.reportes);
      $("#menu-list-item1").css("background-color", "transparent");
      $("#menu-list-item2").css("background-color", "transparent");
      $("#menu-list-item3").css("background-color", "transparent");
      $("#menu-list-item7").css("background-color", "rgba(255, 255, 255, 0.4)");
      $("#menu-list-item8").css("background-color", "transparent");
      $("#menu-list-item9").css("background-color", "transparent");
      $("#menu-list-item10").css("background-color", "transparent");

    };

    $rootScope.soloParques = function(){
      $scope.map.clear();
    $scope.map.off();
    serviceMap.markerParques($scope.map, $scope.reportes);
      $("#menu-list-item1").css("background-color", "transparent");
      $("#menu-list-item2").css("background-color", "transparent");
      $("#menu-list-item3").css("background-color", "transparent");
      $("#menu-list-item7").css("background-color", "transparent");
      $("#menu-list-item8").css("background-color", "rgba(255, 255, 255, 0.4)");
      $("#menu-list-item9").css("background-color", "transparent");
      $("#menu-list-item10").css("background-color", "transparent");

    };
    $rootScope.soloLimpia = function(){
      $scope.map.clear();
    $scope.map.off();
    serviceMap.markerLimpia($scope.map, $scope.reportes);
      $("#menu-list-item1").css("background-color", "transparent");
      $("#menu-list-item2").css("background-color", "transparent");
      $("#menu-list-item3").css("background-color", "transparent");
      $("#menu-list-item7").css("background-color", "transparent");
      $("#menu-list-item8").css("background-color", "transparent");
      $("#menu-list-item9").css("background-color", "rgba(255, 255, 255, 0.4)");
      $("#menu-list-item10").css("background-color", "transparent");

    };

  $rootScope.soloAccesibilidad = function(){
      $scope.map.clear();
    $scope.map.off();
    serviceMap.markerAccesibilidad($scope.map, $scope.reportes);
      $("#menu-list-item1").css("background-color", "transparent");
      $("#menu-list-item2").css("background-color", "transparent");
      $("#menu-list-item3").css("background-color", "transparent");
      $("#menu-list-item7").css("background-color", "transparent");
      $("#menu-list-item8").css("background-color", "transparent");
      $("#menu-list-item9").css("background-color", "transparent");
      $("#menu-list-item10").css("background-color", "rgba(255, 255, 255, 0.4)");

    };








});

$scope.$on("$ionicView.afterEnter", function(event, data){
    // handle event

          $ionicLoading.show({
              template: 'Obteniendo Reportes.',
              showBackdrop: false
          })

        

    //serviceMap.vamonosVaquero(User.username);
    $scope.map.addEventListener(plugin.google.maps.event.MAP_READY, function(map){
    //$scope.animateCamera();
    //alert("vas a pedir reportes");
    





    //$scope.ponies = serviceMap.getPonies(User.session_id);
    
      serviceMap.getPonies(function(ponies){

         $scope.reportes = ponies;
        console.log($scope.reportes);
        $scope.reportes = serviceMap.mamada($scope.reportes);
        $scope.reportesUsuario = serviceMap.reportesUsuario($scope.reportes);
        console.log($scope.reportesUsuario);
        console.log($scope.reportes);
        //alert("termino");
        //serviceMap.marker($scope.map, $scope.reportes);
        serviceMap.marker($scope.map, $scope.reportesUsuario);
        $ionicLoading.hide()
      });





      
      //console.log($scope.reportes);
              // Post-preload callback.
         // Post-preload callback.
        // var donePreloading = function () {
        //     // Animate camera.
        //     alert("animateCamera");
 
        });


  });



	$rootScope.side_menu.style.visibility = "hidden";
	serviceMap.hideMap();
	//alert(JSON.stringify(User));
  //alert(User.falso);

		$scope.misReportes = function(){
      $rootScope.misReportes();		
 		};


 	  $scope.todosReportes = function(){
    $rootScope.todosReportes();
    };


  $scope.soloPavimento = function(){
     
       $rootScope.soloPavimento();
    //   $scope.map.clear();

    };



  $scope.soloAlumbrado = function(){
    
    $rootScope.soloAlumbrado();

    };

  $scope.soloParques = function(){
    $rootScope.soloParques();

    };
  $scope.soloLimpia = function(){
    $rootScope.soloLimpia();
    };

    $scope.soloAccesibilidad = function(){
      $rootScope.soloAccesibilidad();
    };


	$scope.goToReportDetail = function() {
	// $rootScope.justCameBackFromReportDetail = true;
	serviceMap.vamonos();
	// alert(reporte);
	// $location.url('/tab/map_report_detail/' + reporte);
	};
	$scope.chooseImageSource = function() {
    $state.transitionTo('tab.new_report', null, {reload: true, notify:true});
	};

	$scope.borromapa = function(){
		alert("funcion");
		$scope.map.clear();
	};

	$rootScope.brazil = function(brasil){
		alert("chingado");
	};

	  $scope.toggleLeftSideMenu = function() {
    	$ionicSideMenuDelegate.toggleLeft();
    	$scope.$watch(function () {
    return $ionicSideMenuDelegate.getOpenRatio();
  },
  function (ratio) {
    if (ratio === 1){
      //alert("hola");
      //alert(JSON.stringify($rootScope.side_menu.style.visibility))
      $rootScope.side_menu.style.visibility = "visible";
    } else{
    	$rootScope.side_menu.style.visibility = "hidden";

       //alert(JSON.stringify($rootScope.side_menu.style.visibility))
    }
    
  });

  	};

	$scope.sidemenu = function(){



		if($rootScope.side_menu.style.visibility == "hidden" ){
			$rootScope.side_menu.style.visibility = "visible";
		}
		else
			{$rootScope.side_menu.style.visibility = "hidden";}
	};
	$scope.animateCamera = function(){

		$scope.map.getMyLocation(function(location) {
    $scope.map.animateCamera({
			  'target': new plugin.google.maps.LatLng(location.latLng.lat,location.latLng.lng),
			  'tilt': 60,
			  'zoom': 15,
			  'bearing': 0,
			  'duration': 1000
		});
		});

	};

	$rootScope.marcadores = function() {



		$ionicLoading.show({
           		template: 'Obteniendo Reportes.',
           		showBackdrop: false
        	})



alert($scope.reports.length);




// map.addEventListener(plugin.google.maps.event.MAP_READY, function(){
// 	 map.addEventListener(plugin.google.maps.event.MAP_CLICK, function(){
// 	    
// 	 });

// 	var icons = {
// 		pavimento:'www/img/marcador_idle_pavimento-original.png',
// 		alumbrado:'www/img/marcador_idle_alumbrado-original.png',
// 		parques:'www/img/marcador_idle_parques-original.png',
// 		limpia:'www/img/marcador_idle_limpia-original.png'
// 	};

// 	angular.forEach($scope.reports, function(value, key){
// 		if(value.report_category_id == 2)
// 		{
// 			value.icon = icons.pavimento;
// 		}else if(value.report_category_id == 1){
// 			value.icon = icons.limpia;
// 		}else if(value.report_category_id == 3){
// 			value.icon = icons.parques

// 		}else if(value.report_category_id == 4){
// 			value.icon = icons.alumbrado
// 		}

// 		});

// 	for(var i = 0; i < $scope.reports.length; i++){
// 			map.addMarker({
// 				'index' : i,
// 				'latitud': $scope.reports[i].latitude,
// 				'longitude': $scope.reports[i].longitude,
// 				'image_url': $scope.reports[i].image_url,
// 				'reporte_id': $scope.reports[i].id,
// 				'categoria': $scope.reports[i].get_report_category,
// 				'position': new plugin.google.maps.LatLng($scope.reports[i].latitude, $scope.reports[i].longitude),
// 				'address' : $scope.reports[i].address,
// 				'comentarios' : $scope.reports[i].comments_conversion,
// 				'me_afecta' : $scope.reports[i].votes_conversion,
// 				'icon': {
// 					'url': $scope.reports[i].icon,
// 					'size':{
// 						'width': 42,
// 						'height': 53
// 					}
// 				}
// 			},function(marker){
// 			 marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
// 				$scope.reporte = marker.get('reporte_id'); 

// 			    marker.setAnimation(plugin.google.maps.Animation.BOUNCE);
// 			    marker.hideInfoWindow();
// 			     console.log(marker.get('image_url')); 
// 			     $( ".card").show();
// 			     $( "#valor" ).empty().append( marker.get('index'));
// 			     $( "#reporte_id" ).empty().append( marker.get('reporte_id'));
// 			     $("#categoria").empty().append(marker.get('categoria'));
// 			     $("#comentarios").empty().append(marker.get('comentarios'));
// 			     $("#me_afecta").empty().append(marker.get('me_afecta'));
// 			     $( "#direccion" ).empty().append( marker.get('address') ).ellipsis({ lines: 2 ,
// 			     	responsive: true});
// 			     var img = document.createElement("IMG");
//     				img.src = marker.get('image_url');
// 			     $('#image').html(img); 
// 			     //$('#image').prepend($('<img>',{id:'theImg',src:'theImg.png'}))
// 			  });
// 		})

// 		if(($scope.reports.length - 1)== i)
// 		{	 	
// 		  $timeout(function(){
//     	  $ionicLoading.hide();
//    			 },2000);
// 		}
// 	} //<==== CICLO FOR

// 	}) //<==== READY EVENT MAP READY
	
	 		  $timeout(function(){
   	  $ionicLoading.hide();
   			 },2000);
	

	}; //<=== MARCADORES EVENT



	document.addEventListener("deviceready", function(){

// 		//Reports.init(User.session_id);
// 		Reports.getReports(User.session_id); 




		}); //<< DEVICE READY 

// 		// map.setMyLocationEnabled(true);
// 		// map.getUiSettings().setMyLocationButtonEnabled(false;)
// 		//cordova.plugins.camerapreview.stopCamera()
//  		//map.refreshLayout();

// 	// $scope.$on('$ionicView.enter', function () {
// 	// 	$ionicHistory.clearHistory();
		
// 	// 		Reports.init(User.session_id);
// 	// });



// 	map.addEventListener(plugin.google.maps.event.MAP_READY, function(){
// 	 map.addEventListener(plugin.google.maps.event.MAP_CLICK, function(){
// 	 	//map.setClickable(true);
// 	    $( ".card").hide();
	    

// 	 });


// //-----------------------------------------------------------------------
// 		for(var i = 0; i < $scope.reports.length; i++){
// 			//console.log($scope.reports[i]);
// 			map.addMarker({
// 				'index' : i,
// 				'latitud': $scope.reports[i].latitude,
// 				'longitude': $scope.reports[i].longitude,
// 				'image_url': $scope.reports[i].image_url,
// 				'reporte_id': $scope.reports[i].id,
// 				'categoria': $scope.reports[i].get_report_category,
// 				'position': new plugin.google.maps.LatLng($scope.reports[i].latitude, $scope.reports[i].longitude),
// 				'address' : $scope.reports[i].address,
// 				'comentarios' : $scope.reports[i].comments_conversion,
// 				'me_afecta' : $scope.reports[i].votes_conversion,
// 				'icon': {
// 					'url': $scope.reports[i].icon,
// 					'size':{
// 						'width': 42,
// 						'height': 53
// 					}
// 				}
// 			},function(marker){
// 			 marker.addEventListener(plugin.google.maps.event.MARKER_CLICK, function() {
				
// 				//$scope.searchaddress(marker.get('latitud'), marker.get('longitude'));

// 				$scope.reporte = marker.get('reporte_id'); 

// 			    marker.setAnimation(plugin.google.maps.Animation.BOUNCE);
// 			    marker.hideInfoWindow();
// 			     console.log(marker.get('image_url')); 
// 			     $( ".card").show();
// 			     $( "#valor" ).empty().append( marker.get('index'));
// 			     $( "#reporte_id" ).empty().append( marker.get('reporte_id'));
// 			     $("#categoria").empty().append(marker.get('categoria'));
// 			     $("#comentarios").empty().append(marker.get('comentarios'));
// 			     $("#me_afecta").empty().append(marker.get('me_afecta'));
// 			     $( "#direccion" ).empty().append( marker.get('address') ).ellipsis({ lines: 2 ,
// 			     	responsive: true});
// 			     var img = document.createElement("IMG");
//     				img.src = marker.get('image_url');
// 			     $('#image').html(img); 
// 			     //$('#image').prepend($('<img>',{id:'theImg',src:'theImg.png'}))
// 			  });
// 		})
// 	}

// 	});


///INVOCAS MAPA DOS




});