angular.module('yociudadano')

.controller('ReportDetailCtrl', function($scope, $rootScope, User, Reports, Comments, $stateParams, $ionicModal, $ionicScrollDelegate, $ionicPopup, $http, $ionicTabsDelegate, $cordovaSocialSharing, $rootScope, $cordovaCamera, $ionicPlatform, $ionicActionSheet) {
	// Get current report
	
	$scope.$on('$ionicView.beforeEnter'), function(){
		appointmentsService.getAllAppointments(); 

	};

	Reports.getReportDetails(User.session_id, $stateParams.id).then(function() {
		$scope.report = Reports.current;
		// Configure view classes
		if ($scope.report.user_is_affected) {
			$scope.meAfectaClassName = 'icon-acciones-no-afecta';
			$scope.meAfectaButtonText = 'No me afecta';
		} else {
			$scope.meAfectaClassName = 'icon-acciones-me-afecta';
			$scope.meAfectaButtonText = 'Me afecta';
		}
		if ($scope.report.status == 3) {
			$rootScope.marcarResueltoClassName = 'icon-revision';
			$rootScope.marcarResueltoButtonText = 'En revisión';
		} else if ($scope.report.status == 4) {
			$rootScope.marcarResueltoClassName = 'icon-resuelto';
			$rootScope.marcarResueltoButtonText = 'Completo';
		} else {
			$rootScope.marcarResueltoClassName = 'icon-acciones-completo';
			$rootScope.marcarResueltoButtonText = 'Resuelto';
		}
		Reports.getComments(User.session_id).then(function() {
			$scope.comments = Reports.currentComments;
		});
	}, function(reason) {
		// Could not get report details
		var errorAlert = $ionicPopup.alert({
			title: 'Error',
			template: 'No se pudo obtener la información, inténtalo más tarde.',
			okType: 'button-energized'
		});
	});

	$scope.doRefresh = function() {
		Reports.getReportDetails(User.session_id, $scope.report.id).then(function() {
			$scope.report = Reports.current;
			// Configure view classes
			if ($scope.report.user_is_affected) {
				$scope.meAfectaClassName = 'icon-acciones-no-afecta';
				$scope.meAfectaButtonText = 'No me afecta';
			} else {
				$scope.meAfectaClassName = 'icon-acciones-me-afecta';
				$scope.meAfectaButtonText = 'Me afecta';
			}
			if ($scope.report.status == 3) {
				$scope.marcarResueltoClassName = 'icon-revision';
				$scope.marcarResueltoButtonText = 'En revisión';
			} else if ($scope.report.status == 4) {
				$scope.marcarResueltoClassName = 'icon-resuelto';
				$scope.marcarResueltoButtonText = 'Completo';
			} else {
				$scope.marcarResueltoClassName = 'icon-acciones-completo';
				$scope.marcarResueltoButtonText = 'Resuelto';
			}
			Reports.getComments(User.session_id).then(function() {
				$scope.comments = Reports.currentComments;
			});
		}, function(reason) {
			// Could not get report details
			var errorAlert = $ionicPopup.alert({
				title: 'Error',
				template: 'No se pudo obtener la información, inténtalo más tarde.',
				okType: 'button-energized'
			});
		}).finally(function() {
			$scope.$broadcast('scroll.refreshComplete');
		});
	}

	// Initialize comments
	$scope.newComment = {};
	$scope.newReplyComment = {};

	// Functions
	$scope.voteToggle = function() {
		if ($scope.report.user_is_affected) {
			Reports.removeVote(User.session_id).then(function() {
				$scope.meAfectaClassName = 'icon-acciones-me-afecta';
				$scope.meAfectaButtonText = 'Me afecta';
				$scope.report.votes_conversion = $scope.report.votes_conversion - 1;
				$scope.report.user_is_affected = false;
				// var successAlert = $ionicPopup.alert({
				// 	title: 'Gracias',
				// 	template: 'Tu voto ha sido enviado.',
				// 	okType: 'button-energized'
				// });
			}, function(reason) {
				// Send error alert
				var errorAlert = $ionicPopup.alert({
					title: 'Error',
					template: 'No se pudo enviar tu voto, inténtalo más tarde.',
					okType: 'button-energized'
				});
			});
		} else {
			Reports.addVote(User.session_id).then(function() {
				$scope.meAfectaClassName = 'icon-acciones-no-afecta';
				$scope.meAfectaButtonText = 'No me afecta';
				$scope.report.votes_conversion = $scope.report.votes_conversion + 1;
				$scope.report.user_is_affected = true;
				// var successAlert = $ionicPopup.alert({
				// 	title: 'Gracias',
				// 	template: 'Tu voto ha sido enviado.',
				// 	okType: 'button-energized'
				// });
			}, function(reason) {
				// Send error alert
				var errorAlert = $ionicPopup.alert({
					title: 'Error',
					template: 'No se pudo enviar tu voto, inténtalo más tarde.',
					okType: 'button-energized'
				});
			});
		}
	};


	$scope.proposeCompleted = function() {
			
			if ($scope.report.status == 2) {
			// Reveal action sheet to select image source
			var sourceSheet = $ionicActionSheet.show({
				buttons: [{
					text: 'Cámara'
				}, {
					text: 'Galería de fotos'
				}],
		
				titleText: 'Por favor, proporciona evidencia, elige una opción',
				cancelText: 'Cancelar',
				buttonClicked: function(index) {
					if (index === 0) {
						Reports.tomaDeFoto(User.session_id, Camera.PictureSourceType.CAMERA);
				
					} else if (index === 1) {
						Reports.tomaDeFoto(User.session_id, Camera.PictureSourceType.PHOTOLIBRARY); 
					
					}
					return true;
				}
			});
		}

	};


	$scope.share = function() {


	    $cordovaSocialSharing.share('He compartido un reporte via @planjuarez #YoCiudadano', null, null, 'http://yoreporto.com.mx/reports/' + Reports.current.id)
			.then(function(result){

			}, function(err){
				alert(error);
			});

	};

	$scope.addComment = function(comment) {

		if(comment.content.length <= 1) {
			// Send alert indicating shortness of comment
			$ionicPopup.alert({
					title: 'Comentario muy corto',
					template: 'Favor de agregar un comentario mayor a 20 caracteres.',
					okType: 'button-energized'
				});
		} else {

		
			var usuarionuevo = $scope.report.user_name;
			//alert(User); 		
			 console.log(usuarionuevo);
	 		console.log(User.falso);

 		if(usuarionuevo != User.falso){
//-----------------------------------------------------------------
		   $http({
   			 url: 'https://jbossunifiedpush1-denialtorres.rhcloud.com/ag-push/rest/sender/',
    		dataType: 'json',
    		method: 'POST',
    		data: {  
    			"alias" : [usuarionuevo],
				"message": {
                	"alert": JSON.stringify(usuarionuevo) + " comentó tu reporte",
                	"title": "Title",
                	"action": "Action",
                	"sound": "default",
                	"badge": 2
            			}
            
				},
   			 headers: {
       			 "Content-Type": "application/json",
        		"Authorization": "Basic MzQzMzc4YWUtYzYxMi00Mzg2LWJjNzktN2EyNjVmM2U5NTRhOmU4YzBlMGQxLTNhNzMtNGM0OS04OWI3LThiZTU1ODQzNDRmNw==",
        		"Accept": "application/json"
   				 }

		}) }; 
//------------------------------------------------------------------------------

			Reports.postComment(User.session_id, comment.content, false).then(function() {
				$scope.comments = Reports.currentComments;
				$scope.newComment = {}
				$ionicScrollDelegate.scrollBottom(true);
			}, function(reason) {
				// Send alert error
			});
		}
	}

	$scope.flagComment = function(comment_id) {
		Comments.flag(User.session_id, comment_id).then(function() {
			// Send success alert
		}, function(reason) {
			// Send alert error
		});
	};

	$scope.likeToggle = function(comment) {
		if (comment.liked_by_user) {
			Comments.unlike(User.session_id, comment.id).then(function() {
				comment.liked_by_user = false;
			}, function(reason) {
				// Send alert error
			});
		} else {
			Comments.like(User.session_id, comment.id).then(function() {
				comment.liked_by_user = true;
			}, function(reason) {
				// Send alert error
			});
		}
	}

	$scope.replyComment = function() {
		if($scope.newReplyComment.content.length >= 1) {
			$scope.newReplyComment.content = '@' + $scope.newReplyComment.replyToUsername + '\n' + $scope.newReplyComment.content;
			Reports.postComment(User.session_id, $scope.newReplyComment.content, $scope.newReplyComment.replyToId).then(function() {
				$scope.comments = Reports.currentComments;
				$scope.newReplyComment = {}
				$scope.closeReplyToModal();
				$ionicScrollDelegate.scrollBottom(true);
			}, function(reason) {
				// Send alert error
			});
  
 
					//------------------
 $http({
    url: 'https://jbossunifiedpush1-denialtorres.rhcloud.com/ag-push/rest/sender/',
    dataType: 'json',
    method: 'POST',
    data: {
    		"alias" : [$scope.newReplyComment.replyToUsername],
			"message": {
                "alert": User.falso + " respondió tu comentario",
                "title": "Title",
                "action": "Action",
                "sound": "default",
                "badge": 2
            		}
            
},
    headers: {
        "Content-Type": "application/json",
        "Authorization": "Basic MzQzMzc4YWUtYzYxMi00Mzg2LWJjNzktN2EyNjVmM2U5NTRhOmU4YzBlMGQxLTNhNzMtNGM0OS04OWI3LThiZTU1ODQzNDRmNw==",
        "Accept": "application/json"
    }

}).success(function(response){
	alert("funciono");
    $scope.response = response;
}).error(function(error){
    $scope.error = error;
});
//-------

		} 



		else {
			// Send alert indicating shortness of comment
		}		
	};

	$ionicModal.fromTemplateUrl('templates/reply_to_modal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.replyToModal = modal;
	});

	$scope.closeReplyToModal = function() {
		$scope.newReplyComment = {};
		$scope.replyToModal.hide();
	};

	$scope.openReplyToModal = function(postingUserId, postingUserUsername) {
		$scope.newReplyComment.replyToId = postingUserId;
		$scope.newReplyComment.replyToUsername = postingUserUsername;
		$scope.replyToModal.show();
	};

		$scope.selectEvidence = function (source) {

		$rootScope.selectEvidence(); 

		
	}


	$scope.proposeCompletedo = function (source) {
		$ionicPlatform.ready(function() {
			var options = {
				quality: 40,
				correctOrientation: true,
				mediaType: 0,
				sourceType: source
			};

			$cordovaCamera.getPicture(options).then(function(imageData) {
				$rootScope.windowData.show = false;
				Reports.proposeCompleted(User.session_id).then(function() {
					$scope.marcarResueltoButtonText = 'En revisión'; 
				    $scope.marcarResueltoClassName = 'ion-android-time';
					$rootScope.marcarResueltoButtonText = 'En revisión';
					$rootScope.marcarResueltoClassName = 'ion-android-time';
					

					var successAlert = $ionicPopup.alert({
						title: 'Gracias',
						template: 'El reporte será revisado.',
						okType: 'button-energized'
					}).then(function () {
						// Recover window
						$rootScope.windowData.show = true;
					});
				}, function(reason) {
					// Send error alert
					var errorAlert = $ionicPopup.alert({
						title: 'Error',
						template: 'No se pudo marcar como resuelto, inténtalo más tarde.',
						okType: 'button-energized'
					}).then(function () {
						// Recover window
						$rootScope.windowData.show = true;
					});
				});
			}, function(reason) {
				// Something went wrong with da picture
				// Don't send alert, it will appear if user cancels operation
				var errorAlert = $ionicPopup.alert({
					title: 'Error',
					template: 'No se pudo obtener acceso a las imágenes. Favor de revisar permisos en las configuraciones de la aplicación.',
					okType: 'button-energized'
				});
			});
		});
	}




})
