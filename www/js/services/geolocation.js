angular.module('yociudadano')

.factory('Geolocation', function(
	$q,
	$ionicPlatform,
	$http,
	$cordovaGeolocation
) {
	var o = {
		fallbackPositionObject: {
			latitude: '31.677927115318656',
			longitude: '-106.43348693847656',
			accuracy: 0
		}
	};

	o.getCurrentPosition = function(options) {
		var defer = $q.defer();
		options = options || {
			timeout: 10000,
			maximumAge: 10000,
			enableHighAccuracy: true
		};

		$ionicPlatform.ready(function () {
			$cordovaGeolocation.getCurrentPosition(options).then(function (position) {
				defer.resolve(position);
			}, function (locationError) {
				alert("Activa el GPS de tu dispositivo para poder visualizar el mapa");
				defer.reject({
					code: locationError.code,
					message: locationError.message,
					coords: o.fallbackPositionObject
				});
			});
		});
		return defer.promise;
	};

	o.getDefaultPosition = function () {
		return o.fallbackPositionObject;
	};

	return o;
})