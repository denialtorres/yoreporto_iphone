angular.module('yociudadano')

.factory("Reports", function($http, SERVER, $cordovaFileTransfer, $ionicLoading, $rootScope, uiGmapGoogleMapApi, $ionicActionSheet,  $q, serviceMap, $cordovaCamera) {
	var o = {
		list: [],
		current: false,
		currentWindowReport: false,
		currentComments: [],
		newReport: {
			image: false,
			details: '',
			latitude: false,
			longitude: false,
			report_category_id: false
		},
		justCreatedANewReport: false
	}

	var imagenUrlReporte; 

	o.getReports = function(session_id) {
		//alert("aqui generas los reportes bebe");
		var req = {
			method: 'GET',
			url: SERVER.url + '/api/v1/reports',
			headers: {
				'SessionId': session_id
			}
		};

		var deferred = $q.defer(); 

		//alert("incias proceso"); 
		// return $http(req)
		// 	.success(function(data) {
		// 	alert("exito");
		// 	console.log(data.length);
		// 	dato = data.slice(-100);
		// 	console.log(dato.length);
		// 	angular.forEach(data, function(value, key) {
		// 		value.image_url = SERVER.url + value.image_url;
		// 		var value1 = value.image_url;
		// 		var res = value1.replace("/original/", "/medium/");
		// 		value.image_url = res; 
		// 		o.list.push(value);
		// 	});
		// 	alert(o.list.length);
		// });
		
		$http(req)
			.success(function(data){
			// alert(data.length);
			 	console.log(data.length);
			dato = data.slice(-100);
			console.log(dato.length);
			angular.forEach(data, function(value, key) {
				value.image_url = SERVER.url + value.image_url;
				var value1 = value.image_url;
				var res = value1.replace("/original/", "/medium/");
				value.image_url = res; 
				o.list.push(value);
			});
			 		
			})
			.finally(function() {
  				console.log("finally finished repos");
  				alert("algo");
			});
		//return "gato"; 

	}

	o.getReportDetails = function(session_id, report_id) {
		var req = {
			method: 'GET',
			url: SERVER.url + '/api/v1/reports/' + report_id,
			headers: {
				'SessionId': session_id
			}
		};

		return $http(req).success(function(data) {
			o.current = data;
			o.current.image_url = SERVER.url + o.current.image_url;
		});
	}

	o.setCurrentReport = function(report) {
		o.current = report;
		return o.current;
	}

	o.setCurrentWindowReport = function(report, marker) {
		o.currentWindowReport = { report: report, marker: marker };
		return o.currentWindowReport;
	}

	o.getComments = function(session_id) {
		o.currentComments = [];

		var req = {
			method: 'GET',
			url: SERVER.url + '/api/v1/reports/' + o.current.id + '/comments',
			headers: {
				'SessionId': session_id
			}
		};

		return $http(req).success(function(data) {
			angular.forEach(data, function(value, key) {
				if(value.posting_user_avatar) {
					var facebookre = /facebook/;
					var twitterre= /pbs.twimg.com/; 
					if(!facebookre.test(value.posting_user_avatar)) {
						if(!twitterre.test(value.posting_user_avatar)){
							value.posting_user_avatar = SERVER.url + value.posting_user_avatar;
						}
					}
					console.log(value.posting_user_avatar);
				} else {
					value.posting_user_avatar = 'img/missing.png';
				}
				console.log("comentarios => ");
				console.log(o.currentComments);
				o.currentComments.push(value);
			});
		});
	}

	o.postComment = function(session_id, commentText, replyToId) {
		var params = { content: commentText };
		if (replyToId) {
			params.reply_to_id = replyToId;
		}
		var req = {
			method: 'POST',
			url: SERVER.url + '/api/v1/reports/' + o.current.id + '/add_comment',
			headers: {
				'SessionId': session_id
			},
			data: params
		};

		return $http(req).success(function(data) {
			// push new comment to array
			if(data.posting_user_avatar) {
				var facebookre = /facebook/;
				var twitterre= /pbs.twimg.com/; 
				if(!facebookre.test(data.posting_user_avatar)) {
					if(!twitterre.test(data.posting_user_avatar)){
					data.posting_user_avatar = SERVER.url + data.posting_user_avatar; }
				}
			} else {
				data.posting_user_avatar = 'img/missing.png';
			}
			o.currentComments.push(data);
		});
	}

	o.addVote = function(session_id) {
		var req = {
			method: 'POST',
			url: SERVER.url + '/api/v1/reports/' + o.current.id + '/add_vote',
			headers: {
				'SessionId': session_id
			}
		};

		return $http(req);
	}

	o.removeVote = function(session_id) {
		var req = {
			method: 'DELETE',
			url: SERVER.url + '/api/v1/reports/' + o.current.id + '/remove_vote',
			headers: {
				'SessionId': session_id
			}
		};

		return $http(req);
	}

	o.proposeCompleted = function(session_id) {

		//alert("te conectas a imgur");
        // alert("declaraste opciones de la imagen");
       

		var sourceSheet = $ionicActionSheet.show({
			buttons: [
			{ text: 'Cámara' },
			{ text: 'Galería de fotos' }
			],
			titleText: 'Por favor, proporciona evidencia, elige una opción',
			cancelText: 'Cancelar',
			buttonClicked: function(index){
				if (index === 0){ 
					//alert("camara");
					//aqui va las funciones declaradas para la camara
					o.tomaDeFoto(session_id, Camera.PictureSourceType.CAMERA); 


					//aqui terminan


				} else if (index === 1){
					//alert("galeria");
					o.tomaDeFoto(session_id, Camera.PictureSourceType.PHOTOLIBRARY);  
				}

				return true; 
			} 
		});
 
	}

	o.tomaDeFoto = function(session_id, recurso){
		//alert("estas en toma de foto");
		 var optiones = {
            quality : 75,
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType : recurso,
            allowEdit : true,
            encodingType: Camera.EncodingType.JPEG,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false
        };
     
     $cordovaCamera.getPicture(optiones).then(function(imageData) {
            console.log(imageData);
            $ionicLoading.show({template: "Subiendo..."});
			//   "Authorization": "Bearer " + imgurInstance.getAccessToken()
			var  clientId = "319aaa6d9162af7"; 
            var options = {
                fileKey: "image",
                fileName: "image.jpg",
                chunkedMode: false,
                mimeType: "image/jpeg",
                headers: {
							"Authorization" : "Client-ID " + clientId 
                }
            };
            $cordovaFileTransfer.upload("https://api.imgur.com/3/image", imageData, options).then(function(result) {
                console.log(result);
				console.log(result.response.data);
				var respuesta = result.response;
		//		alert(JSON.stringify(respuesta));
				var data = JSON.parse(respuesta);
				imagenUrlReporte = data.data.link;
		//		alert(JSON.stringify(data.data.link));
            }, function(error) {
                console.error(error);
            }).then(function() {
		//		alert("funciono");
                $ionicLoading.hide();
          //       alert("ya tomaste una foto y tienes la URL, la url es " + imagenUrlReporte);
                 var requ = {
					data: { 'evidence' : imagenUrlReporte},
					method: 'PUT',
					url: SERVER.url + '/api/v1/reports/' + o.current.id,
					headers: {
						'SessionId': session_id
					}
				}


			  $http(requ);
			  	$rootScope.marcarResueltoButtonText = 'En revisión'; 
				$rootScope.marcarResueltoClassName = 'ion-android-time';

			return true; 



            });
        }, function(error) {
            console.error(error);
        });


	}

	o.saveNewReportImage = function(imageURI) {
				//alert("estas aqui");
		o.newReport.image = imageURI;
		$('.new-report-image-preview').css("background-image", "url(" + o.newReport.image + ")");
		$('.new-report-image-preview').css("background-repeat", "round");
		$('#logo1').css('zIndex', '-1');
		$('#logo').hide();
		//$('.titulo').css("visibility", "hidden");
		//alert("acabas");
	}

	o.submitNewReport = function(session_id, data) {
		o.justCreatedANewReport = false;

		var options = {
			fileKey: 'report_api[image]',
			headers: {
				'SessionId': session_id
			},
			params: {
				'report_api[details]': data.details,
				'report_api[latitude]': data.latitude,
				'report_api[longitude]': data.longitude,
				'report_api[report_category_id]': data.report_category_id,
				'report_api[username]' : 'user'
			}
		};

		var url = SERVER.url + '/api/v1/reports/'
		//alert(data.image);
		console.log(data.image);
		return	$cordovaFileTransfer.upload(url, data.image, options);
	}

	o.submitReportResult = function(status) {
		o.justCreatedANewReport = status;
	}

	o.emptyReportList = function() {
		o.list = [];
	}

	o.init = function(session_id) {
		if (o.justCreatedANewReport) {
			o.list = [];
		}

		if (o.list.length === 0) {
			alert("deberias estar aqui");
			return o.getReports(session_id);
		}
	}

	return o;
})