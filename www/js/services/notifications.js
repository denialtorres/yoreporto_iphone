angular.module('yociudadano')

.factory("Notifications", function($http, SERVER, $rootScope, $q, User) {
	var o = {
		list: [],
		unread: 0
	}

	o.getNotifications = function(session_id) {

		//console.log("estas en getNotifications");
		var req = {
			method: 'GET',
			url: SERVER.url + '/api/v1/notifications',
			headers: {
				'SessionId': session_id
			}
		};

		return $http(req).success(function(data) {
			//console.log(data.length);
			o.list = [];
			
			//console.log("1. Obtuviste el ancho de los datos generados");

			angular.forEach(data, function(value, index) {
				//console.log("2. Vas a analizar los datos uno por uno");
				if (value.get_user_avatar) {
					//console.log("3a. Tiene Avatar");
					if (!value.is_read) {
						//alert(o.unread);
						//alert(1);
						//$rootScope.notificador()

						o.unread++;
						//console.log("unread = " + o.unread);
							
						$rootScope.badgeNotification(o.unread);	
					}
					var facebookre = /facebook/;
					var twitterre= /pbs.twimg.com/; 
					if (!facebookre.test(value.get_user_avatar)) {
						//console.log("3b. Tiene Avatar de Facebook")
						if(!twitterre.test(value.get_user_avatar)){
							//console.log("hola")
							value.get_user_avatar = SERVER.url + value.get_user_avatar;
							}
						}
						
				} else {
					//console.log("3c. No tiene avatar");
					if (!value.is_read) {
						//alert(o.unread);
						//alert(1);
						//$rootScope.notificador()

						o.unread++;
						//console.log("unread = " + o.unread);
							
						$rootScope.badgeNotification(o.unread);	
					}
					value.get_user_avatar = 'img/YoCiudadanoAdmin.png';
				}

				o.list.push(value); 
				// if(value.get_username != User.falso)
				// 	{ o.list.push(value); };
				
			});


		});

	}


	o.getNotifications1 = function(session_id) {

		console.log("estas en getNotifications");
		var req = {
			method: 'GET',
			url: SERVER.url + '/api/v1/notifications',
			headers: {
				'SessionId': session_id
			}
		};

		return $http(req).success(function(data) {
			//console.log(data.length);
			console.log("Extraes notificaciones desde la pestaña del mapa");
			o.list = [];

			

			angular.forEach(data, function(value, index) {
				if (value.get_user_avatar) {
					if (!value.is_read) {
						//alert(o.unread);
						//alert(1);
						//$rootScope.notificador();
						o.unread = 0;

						o.unread++;
					}
					var facebookre = /facebook/;
					var twitterre= /pbs.twimg.com/; 
					if (!facebookre.test(value.get_user_avatar)) {
						console.log("3b. Tiene Avatar de Facebook")
						if(!twitterre.test(value.get_user_avatar)){
							console.log("hola")
							value.get_user_avatar = SERVER.url + value.get_user_avatar;
							}
						}
				} else {
					value.get_user_avatar = 'img/YoCiudadanoAdmin.png';
				}

				o.list.push(value); 
				// if(value.get_username != User.falso)
				// 	{ o.list.push(value); };
				
			});


		});

	}

	o.markNotificationAsRead = function(session_id, notification_id) {
		//console.log("estas marcadon notificacion como leida");
		var req = {
			method: 'POST',
			url: SERVER.url + '/api/v1/notifications/' + notification_id + '/mark_as_read',
			headers: {
				'SessionId': session_id
			}
		};

		return $http(req);
	}

	o.init = function(session_id) {
		if (o.list.length === 0) {
			return o.getNotifications();
		}
	}

	o.refresh = function(session_id) {

		return o.getNotifications(session_id);
	}

	o.unreadCount = function() {

		return o.unread;
	}

	o.casa = function(){

		return 6; 
	}

	return o;
})