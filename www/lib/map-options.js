(function(window, google, mapster){
  mapster.MAP_OPTIONS = {
    center: {
      lat:31.739444,
      lng: -106.486944
    },
    zoom: 10,
    disableDefaultUI: true,
    scrollwheel: true,
    draggable: true,
    styles:[{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"color":"#cdd9f0"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"lightness":20},{"visibility":"on"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#1d1d1d"}]},{"featureType":"landscape","elementType":"geometry.stroke","stylers":[{"color":"#888585"},{"weight":"1.00"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffcd11"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#eb9132"},{"lightness":"0"},{"weight":"1.00"}]},{"featureType":"road.highway","elementType":"labels.text.fill","stylers":[{"color":"#fef6f6"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"lightness":18},{"visibility":"on"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#fcaa27"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#363023"}]},{"featureType":"road.arterial","elementType":"labels.text.fill","stylers":[{"color":"#f9eeee"}]},{"featureType":"road.arterial","elementType":"labels.text.stroke","stylers":[{"weight":"2.93"}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"lightness":16}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"#63646d"}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#fffefe"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#124469"},{"lightness":17}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#bfcae5"}]}],
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    cluster:{
      options:{
        styles:[{
          url: 'http://google-maps-utility-library-v3.googlecode.com/svn/tags/markerclustererplus/2.1.2/images/m5.png',
          height: 90,
          width: 89,
          textColor: '#FFF',
          textSize: 18
        }]
      }
    }
    
  };
  
}(window, google, window.Mapster || (window.Mapster = {})))